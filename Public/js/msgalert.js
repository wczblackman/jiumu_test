/* seconds:倒计时；okBtnVisible:确定按钮；closeBtnVisible:关闭按钮*/
  function msgalert(title, seconds, okBtnVisible, closeBtnVisible){
    // 用户按钮取消定时器
    var isStopInterval = 1;
    /* “确定”和“关闭”两个按钮是否存在进行相关处理 */
    if(!okBtnVisible) {
        $(".txtbtn").css("display", "none");
    } else {
        $("#windowclosebutton").click(function () {
            $("#windowcenter").slideUp(500);
            isStopInterval = 0;
        });
        $("#cancelorder").click(function() {
	        //询问框
			layer.open({
		    	content: '您确定要取消当期配送吗？'
				,btn: ['确定取消配送', '关闭对话框']
				,yes: function(index){
					location.href = cancelOrderUrl;
					layer.close(index);
		    	}
		  	});
	        
        });
    }
    if(!closeBtnVisible) {
        $("#alertclose").css("display", "none");
    } else{
        $("#alertclose").click(function () {
            $("#windowcenter").slideUp(500);
        });
    }
    // 先提示框Toggle出来
    $("#windowcenter").slideToggle("slow");
    // 如果有倒计时，加入
    if(seconds) {
        var time = seconds;
        // 先把内容添加，再定时器启动，否则有延迟
        $("#txt").html(title+'<br/>页面将在'+time+"后关闭");
        var interval = setInterval(function(){
                                            if(!isStopInterval){
                                                clearInterval(interval);
                                            }
                                            time = time - 1;
                                            $("#txt").html(title+'<br/>页面将在'+time+"后关闭");
                                            console.log(title+'页面将在'+time+"后关闭");
                                            if(time == 0) {
                                                clearInterval(interval);
                                                wx.closeWindow();
                                            }
                                        }, 1000);
    } else {
        $("#txt").html(title);
    }
  }