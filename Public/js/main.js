$.fn.amount = function(num, callback, ftype){
	var num = typeof num === 'undefined' ? 0 : num,
		callback = callback || $.noop,
		isShow = num > 0 ? '' : ' style="display:none;"',
		activeClass = 'active';
	//var ftype = ftype;
	// 对checkbox的check事件
	function check() {
		// 选中即+，非选中即-, 这里的is是点击完之后
		if($(this).is(':checked')){
			//$(_that).val(1);
			add(this);
        }else{
            //$(_that).val(0);
            delCheck(this);
        }
	}

	// +/- 事件
	/*
	 *  获取当前这个菜品的数量值，对是否可进行+进行判断
	 *  设置购物车的数量
	 */
	function add(obje){
		if(ftype == 'opfood' || ftype == 'plusfood') {
			var obj = $(this).prev();	// +号前元素-span
			var _num = obj.find('.num'),
				curNum = parseInt(_num.text(), 10);
		} else if(ftype == 'colfood') {
			var obj = $(obje);			// checkbox
			var _num = obj,
				curNum = parseInt(_num.val(), 10);
		}

		// 获取购物车(左下角的数值)
		var	_cartNum = $('#cartNum'),
			cartNum = _cartNum.text();
			
		var data_obj = obj.parent();
		var max = data_obj.attr("max");/**控制每个菜最多可点多少份**/
		var foodType = data_obj.attr("type");
		var fid = parseInt(data_obj.attr("fid"));
		var fname = data_obj.attr("name");
		var sortid = parseInt(data_obj.attr("data-sort"));
		/* 弃用
		var defchs = parseInt(data_obj.attr("data-defchs"));
		var reqchs = parseInt(data_obj.attr("data-reqchs"));
		*/
		var price = parseFloat(data_obj.attr("data-price"));	// 单价
		var instock = parseInt(data_obj.attr("max"));		// 库存值
		var fgroup = parseInt(data_obj.attr("data-fgroup"));	// 套餐分组
		// 获取当前已点队列的数量
		var foodsStatis = getFoodsAllStatistics();
		var flimitnum = data_obj.attr("data-limitnum");
		// 如果初始化完成 且 未确认超额支付，对用户进行提示
		if(doneInit && !isExcess){
			console.log("初始化完成 且 未确认超额支付");
			// 当前选中套餐的金额
			var curColMoney = 0;
			for(var i=0; i<colPriceList.length;i++) {
				if(colPriceList[i]['colname'] == orderColName) {
					curColMoney = colPriceList[i]['money'];
				}
			}
			/*
			console.log("当前菜品的类型 => " + ftype);
			console.log("当前套餐和备选总金额 =>" + foodsStatis['colAndOpMoneyCount']);
			console.log("当前所属套餐所属金额累计 => " + curColMoney);
			*/
			/* 	1.另购菜品直接提醒
			 *	2.添加备选如果超过套餐规定金额，也进行提示
			 */
			if ((ftype == 'plusfood') || (ftype == 'opfood' && foodsStatis['colAndOpMoneyCount'] >= curColMoney)) {
				if(confirm("您即将超出套餐金额，您要另外购买吗？")) {
					isExcess = true;
				} else {
					return false;
				}
			}
		}
		// 如果是分组菜品，查询是否已有相同分组菜品已经被选择，如果有提示不能选择，先去掉原先的菜品再选择
		if(fgroup > 0) {
			var isChs = false;
			var fnamesStr = '';
			$(".fgroup"+fgroup).each(function(){
				var foodNum = $(this).find(".fcheck").val();
				var foodName = $(this).find(".fcheck").next().val();
				fnamesStr += foodName + ',';
				if(foodNum >= 1) {
					isChs = true;
				}
			});
			
			if(isChs) {
				alert(fnamesStr+"只能选一种哦！");
				obj.attr("checked",false);
				return false;
			}
		}
		// 另购区的限购数控制
		if (ftype == 'opfood') {
			if (flimitnum>0 && curNum+1 > flimitnum) {
				alert(fname + "限购："+flimitnum+"份!");
				return false;
			}
		}
		if(instock < curNum+1) {
			alert("不能大于库存数哦！");
			return false;
		}
		
		if(null != max && max != "" && max != "-1" && curNum >= max){
			return false;
		}
		
		// 当前菜品添加到队列foodsArr中去
		var curFoodsArrLength = foodsArr.length;
		var isNew = true;
		for(var i = 0; i<curFoodsArrLength; i++) {
			// 如果已有，更新数量
			if(foodsArr[i]['fid'] == fid && foodsArr[i]['ftype'] == ftype) {
				foodsArr[i]['num'] += 1;
				isNew = false;
				break;
			} 
			
		}
		if(isNew) {
			// {菜品id, 分类id, 菜品类型, 数量, 菜品名, 价格}
			var newItem = {'fid':fid, 'sort':sortid, 'ftype':ftype, 'num':curNum+1, 'fname':fname, 'price':price};
			foodsArr.push(newItem);
		}
		
		
		// 如果是备选 | 另购，表单提交input class=number => +1
		// 如果是套餐 直接设置checkbox的value为1
		if(ftype == 'opfood' || ftype == 'plusfood') {
			_num.text(++curNum);
			data_obj.next(".number").val(curNum);
			if(curNum > 0){
				obj.show();
				$(this).addClass(activeClass);
			}
		} else if(ftype == 'colfood') {
			_num.val(1);
		}
		var fSt = getFoodsAllStatistics();
		
		return callback.call(obj, '+');
	}

	/*
	 * del 与add对于obj的初始复制不同：
	 * del是后来添加的元素，无法获取amountcb的ftype值，只能这么处理
	 */
	function delCheck(obje){
		var obj = $(obje);
		del(obj);
	}

	function delBtn() {
		var obj = $(this).parent();
		del(obj);
	}
	
	function del(obj){
		//var obj = $(this).parent();
		var ftype = obj.parent().data('ftype');
		var price = parseFloat(obj.parent().data('price'));
		if(ftype == 'opfood' || ftype == 'plusfood') {
			var _num = obj.find('.num'),	// -/+之间的数值
				_add = obj.next(),
				curNum = parseInt(_num.text(), 10);
		} else if(ftype == 'colfood') {
			var _num = obj,				// checkbox
				curNum = _num.val();	// checkbox value
		}
		var data_obj = obj.parent();	// 放数据的div class = order-num
		var fid = data_obj.attr("fid");
		var credit = data_obj.data("credit");
		var ftype = data_obj.data("ftype");
		var fname = data_obj.attr("name");
		// 设置 -/+ 或者checkbox的值
		if(ftype == 'opfood' || ftype == 'plusfood') {
			// 设置-+中间的显示数值
			_num.text(--curNum);
			// 设置input class=number的数值
			obj.parent().next(".number").val(curNum);
		} else if(ftype == 'colfood'){
			_num.val(0);
		}
		// 减少说明foodsArr数组中肯定有这个菜品,因为“-”操作肯定有
		var isZero = false;	// 判断是否这个菜的数量将为0
		var isZeroIndex = 0;
		for(var i = 0; i<foodsArr.length; i++) {
			// 如果已有，更新数量
			if(foodsArr[i]['fid'] == fid && foodsArr[i]['ftype'] == ftype) {
				if(foodsArr[i]['num'] == 1) {
					isZero = true;
					isZeroIndex = i;
					break;
				} else {
					foodsArr[i]['num'] -= 1;
				}
			}
		}
		if(isZero) {
			foodsArr.splice(isZeroIndex, 1);
		}
		if(ftype == 'opfood' || ftype == 'plusfood') {
			if(curNum < 1){
				obj.hide();
				_add.removeClass(activeClass);
			}else{
				_add.addClass(activeClass);
			}
		}
		
		var fSt = getFoodsAllStatistics();
		return callback.call(obj, '-');
	}

	return this.each(function(){
		if(ftype == 'opfood' || ftype == 'plusfood') {
			$(this).before('<span'+ isShow +'><a href="javascript:void(0);" class="btn del '+ activeClass +'"></a><span class="num">'+ num +'</span></span>');
			// bind函数也会将this作为参数传入add方法作为第一个参数
			$(this).bind('click', add);
			$(this).prev().find('.del').bind('click',delBtn);
	
			if(num > 0){
				$(this).addClass(activeClass);
			}
		}
		if(ftype == 'colfood') {
			$(this).bind('click', check);
		}
	});
}
var cbcount = 0;
$.amountCb = function(){
	var _condition = $('#sendCondition'),
		_condition1 = $('#sendCondition1'),
		_creditTotal = $('#creditTotal'),
		_total = $('#totalPrice'),		// 去掉价格 
		_cartNum = $('#cartNum'),
		sendCondition = parseFloat(_condition.text()).toFixed(3);
	// 根据 +/- 调整部分显示
	return function(sign){
		var totalPrice = parseFloat(_total.text()) || 0,
			disPrice = parseFloat(sign + 1) * parseFloat($(this).parents('li').find('.unit_price').text()),
			price = totalPrice + disPrice,
			number = _cartNum.text() == '' ? 0 : parseInt(_cartNum.text()),
			disNumber = number + parseInt(sign + 1);
			price = parseFloat((price).toFixed(3));
		_total.text(price);
		
		var foodsStatis = getFoodsAllStatistics();
		var allNumTotal = foodsStatis['allNumCount'];
		var disNumber = foodsStatis['allNumCount'];
		var colAndOpMoneyCount = parseInt(foodsStatis['colAndOpMoneyCount'],10);
		var plusMoneyCount = parseInt(foodsStatis['plusMoneyCount'], 10);
		// 对底部是否显示进行判断
		// 金额显示
		var colopPrice = $("#colopPrice");
		var plusPrice = $("#plusPrice");
		var extraPrice = $("#extraPrice");
		
		var curColMoney = 0;
		for(var i=0; i<colPriceList.length;i++) {
			if(colPriceList[i]['colname'] == orderColName) {
				curColMoney = colPriceList[i]['money'];
			}
		}
		// 如果套餐+备选小于 套餐金额，显示真实值，比如套餐+备选刚好==200
		if(colAndOpMoneyCount <= colMoneyTotal) {
			if(plusMoneyCount > 0) {
				extraPrice.parent().css("display", "block");
				var dpPrice = plusMoneyCount;
				extraPrice.text(dpPrice);
			} else {
				extraPrice.parent().css("display", "none");
			}
		}	// 如果套餐+备选有其他菜，按照蔬菜原价计算 
		else if (colAndOpMoneyCount > colMoneyTotal) {
			extraPrice.parent().css("display", "block");
			var dpPrice = colAndOpMoneyCount + plusMoneyCount - colMoneyTotal;
			extraPrice.text(dpPrice);
		}
		
		// 如果套餐+备选少于套餐金额，进行提示，隐藏“提交订单”按钮
		if(foodsStatis['colAndOpMoneyCount'] >= colMoneyTotal) {
			_condition.parent().hide().next().show();
		} else {
			_condition.text('套餐未选满');
			_condition.parent().show().next().hide();
		}
		// 采样数
		if(disNumber > 0){
			_cartNum.addClass('has_num').text(allNumTotal);
		} else {
			_cartNum.removeClass('has_num').text('');
		}
		return false;
	}
}

$(function(){
	if($('#swipeNum').length){
		new Swipe($('#imgSwipe')[0], {
			speed: 500, 
			auto: 5000, 
			callback: function(index){
				$('#swipeNum li').eq(index).addClass("on").siblings().removeClass("on");
			}
		});
	}

	$('#storeList li').click(function(e){
		if(e.target.tagName != 'A'){
			location.href = $(this).attr('href');
		}
	});
});
