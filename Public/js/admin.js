
//获取顶部选项卡总长度 不是选项卡时可不用
function tabNavallwidth(){
	var taballwidth=0,
		$tabNav = $(".acrossTab"),
		$tabNavWp = $(".tabNav-wp"),
		$tabNavitem = $(".acrossTab li"),
		$tabNavmore =$(".tabNav-more");
	if (!$tabNav[0]){return}
	$tabNavitem.each(function(index, element) {
        taballwidth+=Number(parseFloat($(this).width()+60))});
	$tabNav.width(taballwidth+25);
	var w = $tabNavWp.width();
	if(taballwidth+25>w){
		$tabNavmore.show()}
	else{
		$tabNavmore.hide();
		$tabNav.css({left:0})}
}

//左侧菜单响应式 宽小于768隐藏
function Scasidedisplay(){
	if($(window).width()>=768){
		$(".aside").show();
	} 
}

$(function(){

	//根据iframe的src 刷新主框内的菜单 2016-05-19 byGS
	var now_src = window.top.document.getElementById('main').contentWindow.location.pathname;
	var sub_m = $('#main-menu a[_href="'+now_src+'"]',window.top.document);
	var top_menu = $('#top-sub-menu',window.top.document);
	if(sub_m.length == 1){

		$('#main-menu a[href]',window.top.document).removeClass('on');
		$('#main-menu dt',window.top.document).removeClass('selected');
		sub_m.addClass('on');
		sub_m.parents('dl').find("dt").addClass('selected');
		$('#main-menu dd',window.top.document).hide();
		sub_m.parents('dl').find("dd").show();

		$('#main-menu i.menu-icon',window.top.document).css("color","#A0A7B1");
		sub_m.parents('dl').find("dt i.menu-icon").css("color","#003A6D");
		$('#main-menu i.menu-icon:first',window.top.document).css("color","#A0A7B1");
		if(sub_m.parents('dd').length == 1){
			var far_menu_n = sub_m.parents('dl').find('dt').text();
			top_menu.text(' > '+far_menu_n+' > '+sub_m.text());

		}else{
			top_menu.text('>'+sub_m.text());
		}

	}

	//layer.config({extend: 'extend/layer.ext.js'});
	Scasidedisplay();
	var resizeID;
	$(window).resize(function(){
		clearTimeout(resizeID);
		resizeID = setTimeout(function(){
			Scasidedisplay();
		},500);
	});
	
	$(".nav-toggle").click(function(){
		$(".aside").slideToggle();
	});
	
	$(".aside").on("click",".menu-dropdown dd li a",function(){
		if($(window).width()<768){
			$(".aside").slideToggle();
		}
	});
	
	//默认选中首菜单
	
	
	//左侧菜单折叠效果
	$.Scfold(".menu-dropdown dl dt",".menu-dropdown dl dd","fast",1,"click");
	
	//*header菜单链接选中
	$(".dropDown-menu a[_href]").click(function(){
		var href =$(this).attr('_href');
		var leftmenu = $(".menu-dropdown a[_href='"+href+"']");
		if(leftmenu.parents("dl").find("dd").is(":hidden"))
			leftmenu.parents("dl").find("dt").trigger("click");
		leftmenu.trigger("click");
	});
 
	
	/*视觉优化左侧菜单，首条菜单去掉border-top*/
	$(".menu-dropdown dt:first").css("border-top","1px solid #F3F3F3");
	
	
	//子页面中，表单的隐藏/显示
	$(".widget-box a[data-action=collapse]").click(function(){
    	$(".widget-body").slideToggle("slow");
		$(this).toggleClass("selected");
  	});

  	//修改 byGS 2016-05-19
	$(".aside").on("click",".menu-dropdown a",function(){
		if($(this).attr('_href')){
			/*菜单效果设置
			$(".menu-dropdown a[href]").removeClass("on");
			$(this).addClass("on");
			$(".menu-dropdown i.menu-icon").css("color","#A0A7B1");
			$(this).parents("dl").find("dt i.menu-icon").css("color","#003A6D");
			$(".menu-dropdown i.menu-icon:first").css("color","#A0A7B1");*/
			var href=$(this).attr('_href');
			var _titleName=$(this).html();	
			var topWindow=$(window.parent.document);
			var iframe_box=topWindow.find('#iframe_box');
			var iframeBox=iframe_box.find('.show_iframe');	
			var showBox=iframe_box.find('.show_iframe:first');
			showBox.find('iframe').attr("src",href).load(function(){
				showBox.find('.loading').hide();
			});		
		}
	});	
	/*无选项卡导航 new
	$(".aside").on("click",".menu-dropdown a",function(){
		if($(this).attr('_href')){
			//菜单效果设置
			$(".menu-dropdown a[href]").removeClass("on");
			$(this).addClass("on");
			$(".menu-dropdown i.menu-icon").css("color","#A0A7B1");
			$(this).parents("dl").find("dt i.menu-icon").css("color","#003A6D");
			$(".menu-dropdown i.menu-icon:first").css("color","#A0A7B1");
			var href=$(this).attr('_href');
			var _titleName=$(this).html();	
			var topWindow=$(window.parent.document);
			var iframe_box=topWindow.find('#iframe_box');
			var iframeBox=iframe_box.find('.show_iframe');	
			var showBox=iframe_box.find('.show_iframe:first');
			showBox.find('iframe').attr("src",href).load(function(){
				showBox.find('.loading').hide();
			});		
		}
	});*/
	
	
}); 
/*弹出层*/
/*
	参数解释：
	title	标题
	url		请求的url
	id		需要操作的数据id
	w		弹出层宽度（缺省调默认值）
	h		弹出层高度（缺省调默认值）
*/
function layer_show(title,url,w,h){
	if (title == null || title == '') {
		title=false;
	};
	if (url == null || url == '') {
		url="404.html";
	};
	if (w == null || w == '') {
		w=600;
	};
	if (h == null || h == '') {
		h=($(window).height() - 50);
	};
	layer.open({
		type: 2,
		area: [w+'px', h +'px'],
		fix: false, //不固定
		maxmin: true,
		shade:0.4,
		title: title,
		content: url
	});
}

/*关闭弹出框口*/
function layer_close(){
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
}