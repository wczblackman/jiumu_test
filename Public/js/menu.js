var menu = {
	offsetAry: [0],
	init: function(id){
		var winH = $(window).height(),
			_this = this,
			_icoMenu = $('#icoMenu'),
			_sideNav = $('#sideNav'),
			maxH = winH - (_icoMenu.parent().is(':visible') ? _icoMenu.outerHeight(true) : 0) - 65;

		this.el =  $(id);
		_sideNav.height(maxH);

		if(_sideNav.find('ul').height() > maxH) new Scroller('#sideNav', {scrollX: false});

		$(window).bind('scroll', function(){
			_this.scroll.call(_this);
		});

		$('#icoMenu').click(function(){
			_sideNav.toggle();
		});

		$('.menu_tt h2').each(function(){
			_this.offsetAry.push($(this).offset().top);
		});

		this.el.find('a').click(function(){
			if($(this).attr('class')=='dztj_a'){
				$('#mymenu_lists .nodztj_c').hide();
			    $('#mymenu_lists .dztj_c').show();
			}else{
				$('#mymenu_lists .nodztj_c').show();
			    $('#mymenu_lists .dztj_c').hide();
			}
			$(this).addClass('on').parent().siblings().find('a').removeClass('on');
			$(window).scrollTop(_this.offsetAry[_this.el.find('a').index(this) + 1]);
		});

		//_this.offsetT = this.el.offset().top;	
	},
	getIndex: function(ary, value){
		var i = 0;
		for(; i < ary.length; i++){
			if(value >= ary[i] && value < ary[i + 1]){
				return i;
			}
		}
		return ary.length -1;
	},
	scroll: function(){
		var st = $(document).scrollTop(),
			index = this.getIndex(this.offsetAry, st),
			i = index - 1;

		if(this.curIndex !== index){ // 判断分类是否切换
			
			$('.menu_tt h2').removeClass('menu_fixed');
			this.el.find('a').removeClass('on');
			if(i >= 0){
				this.el.addClass('menu_fixed');
				$('.menu_tt').eq(i).find('h2').addClass('menu_fixed');
				this.el.find('a').eq(i).addClass('on');	
			}else{
				this.el.removeClass('menu_fixed');
			}
			this.curIndex = index;
		}
	}
}
// 是否初始化完成，未完成：add click事件忽略
var doneInit = 0;	//0:未完成 1:完成
var less4Veg = false;

// 返回当前的菜的总份数和总分值
function getCurFoodsCount() {
	var foodsNumCount = 0;
	var foodsCreCount = 0;
	for(var i=0;i<foodsArr.length;i++) {
		foodsNumCount += foodsArr[i]['num'];
		foodsCreCount += foodsArr[i]['credit'] * foodsArr[i]['num'];
	}
	return {'foodsNumCount':foodsNumCount,'foodsCreCount':foodsCreCount};
}

// 返回当前 (套餐+备选 & 另购 & 总数) 数量
function getFoodsAllStatistics() {
	var col_op_NumCount = 0;	// 套餐+备选的数量总和
	var plus_NumCount = 0;		// 另购的数量总和
	var col_op_MoneyCount = 0;	// 套餐+备选的金额
	var plus_MoneyCount = 0;	// 另购的金额
	for(var i=0; i<foodsArr.length;i++) {
		var ftype = foodsArr[i]['ftype'];
		if(ftype == 'colfood' || ftype == 'opfood') {
			col_op_NumCount += foodsArr[i]['num'];
			col_op_MoneyCount += foodsArr[i]['num'] * foodsArr[i]['price'];
		} else if(ftype == 'plusfood') {
			plus_NumCount += foodsArr[i]['num'];
			plus_MoneyCount += foodsArr[i]['num'] * foodsArr[i]['price'];
		}
	}
	var all_NumCount = plus_NumCount+col_op_NumCount;			// 所有菜品数量总和
	var all_MoneyCount = plus_MoneyCount + col_op_MoneyCount;	// 所有菜品金额总和
	return {'plusNumCount':plus_NumCount, 'colAndOpNumCount':col_op_NumCount, 'allNumCount':all_NumCount, 
		'plusMoneyCount':plus_MoneyCount, 'colAndOpMoneyCount':col_op_MoneyCount, 'allMoneyCount':all_MoneyCount};
}

// 返回当前菜的种类数
function getCurFoodsCateCount() {
	return foodsArr.length;
}
// 所有套餐的金额数组
var colPriceList = new Array();	
$(function(){
	menu.init('#menuNav');
	// 根据默选的情况进行模拟:add事件
	// 注意：这里包含了checkbox和a标签，将把所有的套餐checkbox都默认选中如果
	$('#menuWrap .order-num').each(function(){
		var ftype = $(this).data('ftype');
		// 套餐菜品
		if(ftype == 'colfood') {
			var curColName = $(this).parents(".colfoods").attr('colname');
			var newCol = true;
			var price = parseInt($(this).data('price'));
			for(var i=0; i<colPriceList.length; i++) {
				// 如果队列中已有该套餐
				if(colPriceList[i]['colname'] == curColName) {
					colPriceList[i]['money'] += price;
					newCol = false;	
				}
			}
			// 如果是新套餐
			if(newCol) {
				var newColItem = {'colname':curColName, 'money':price};
				colPriceList.push(newColItem);
			}
			var chkBtn = $(this).children(".fcheck");
			// 每个checkbox绑定
			chkBtn.amount(0, $.amountCb(), ftype);
			// 如果是当前的套餐，进行模拟点击
			if(orderColName == curColName) {
				// 是否已有数量
				var origNum = $(this).data('num');
				// origNum只有0|1，因此只需一次点击
				if(origNum) {
					chkBtn.click();
					chkBtn.attr("checked", true);
				}
			}
		}
		// 备选菜品
		if(ftype == 'opfood' || ftype == 'plusfood') {
			var addBtn = $(this).children('.add');
			addBtn.amount(0, $.amountCb(), ftype);
			// 处理如果初始就有数量,说明是订单修改 - 获取add 下的data-num值
			var num = $(this).children('.add').data('num');
			// 如果有初始数量，进行对应数量的点击事件
			for(var i = 0; i < num; i++){
				addBtn.click();
			}
		}
		// 累加到菜品的总数
		foodsCount += 1;
	});

	//msgalert('1.套餐区+备选区的总金额大于套餐金额'+colMoneyTotal+'时，将在套餐金额里扣除<br/>2.另购区的菜品将在此次配送时上门收取,谢谢!', 0, 1, 1);
	msgalert('新一期的九亩套餐点菜开始啦!', 0, 1, 1);
	// 初始化完成
	doneInit = 1;
	
	/*
	 *	初始化完成，
	 * 	如果当前订单的金额大于套餐金额，将允许超额支付isExcess设置为true
	 */
	var fSt = getFoodsAllStatistics();
	// 当前选中套餐的金额
	var curColMoney = 0;
	for(var i=0; i<colPriceList.length;i++) {
		if(colPriceList[i]['colname'] == orderColName) {
			curColMoney = colPriceList[i]['money'];
		}
	}
	// 如果已有订单的累计金额超过套餐金额，说明先前已知晓超额支付
	if(fSt['colAndOpMoneyCount'] > curColMoney) {
		isExcess = true;
	} else {
		isExcess = false;
	}
	
	var _wraper = $('#menuDetail');

	var dialogTarget;
	// 不展示菜品详情
	/*
	$('.menu_list li').click(function(e){
		var _this = $(this),
			F = function(str){return _this.find(str);},
			title = F('h3').text(),
			imgUrl = F('img').attr('url'),
			price = F('.unit_price').text(),
			sales = F('.sales strong').attr('class'),
			saleNum = F('.sale_num').text(),
			info = F('.info').text(),
			saleDesc = F('.salenum').html(),
			unit=F('.order-num').data('unit'),
			_detailImg = _wraper.find('img');

		_wraper.find('.price').text(price).end()
			.find('.sales strong').attr('class', sales).end()
			.find('.sale_desc').html(saleDesc).end()
			.find('.info').text(info);

		_wraper.parents('.dialog').find('.dialog_tt').text(title);

		if(F('.add').length){
			$('#detailBtn').removeClass('disabled').text('来一'+unit);
		}else{
			$('#detailBtn').addClass('disabled').text('已售完');
		}

		if(imgUrl){
			_detailImg.attr('src', imgUrl).show().next().hide();
		}else{
			_detailImg.hide().next().show();
		}

		dialogTarget = _this;
		_wraper.dialog({title: title, closeBtn: true});

	});
	*/
	$('#menuWrap .price_wrap').click(function(e){
		e.stopPropagation();
	});

	$('#detailBtn').click(function(){
		// alert(dialogTarget.find('.unit_price').text());
		if(!$(this).hasClass('detail')){
			dialogTarget.find('.add ').click();
		}
	});
});