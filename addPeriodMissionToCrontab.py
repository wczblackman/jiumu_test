#!/usr/bin/python
# -*- coding: UTF-8 -*-

import MySQLdb
import time
from crontab import CronTab
from sys import argv

script, mod_name = argv

f = open("crontab_log", "a")

db = MySQLdb.connect(host="localhost", port=6603, user="root", password="Chint!9Mu?", db=str(
    mod_name), unix_socket='/opt/lampp/var/mysql/mysql.sock')

cursor = db.cursor()

sql = "select * from `tp_period` where `status` = 2 limit 1"

try:
    cursor.execute(sql)

    result = cursor.fetchone()

    fromtime = result[3]
    # 防止一期的开始时间比当前时间早，将发送消息的时间设定为当前
    curtime = int(time.time())
    if fromtime < curtime:
        # print str(fromtime) + "<" + str(curtime)
        fromtime = curtime

    endtime = result[4]
    ft_date = time.localtime(fromtime)
    et_date = time.localtime(endtime)

    fyear = ft_date.tm_year
    fmonth = ft_date.tm_mon
    fday = ft_date.tm_mday
    fhour = ft_date.tm_hour
    fmin = ft_date.tm_min

    f.write("这期点菜：" + str(fmonth) + "-" + str(fday))

    eyear = et_date.tm_year
    emonth = et_date.tm_mon
    eday = et_date.tm_mday
    ehour = et_date.tm_hour
    emin = et_date.tm_min

    my_user_cron = CronTab(user=True)

    # 执行自动发送消息
    jst_fmin = fmin + 4
    jst_fhour = fhour
    if jst_fmin >= 60:
        jst_fmin = jst_fmin % 60
        jst_fhour = jst_fhour + 1

    job_sendMsg = my_user_cron.new(command='curl http://wx.9mu.net/'+str(
        mod_name)+'/index.php/Wap/AutoMission/sendOrderMsg > /dev/null 2>&1')

    job_sendMsg.set_comment("执行发送消息 - " + str(result[2]))
    job_sendMsg_time = str(jst_fmin) + ' ' + str(jst_fhour) + \
        ' ' + str(fday) + ' ' + str(fmonth) + ' *'

    f.write("\n发送消息：" + job_sendMsg_time)

    jst_star_count = job_sendMsg_time.count('*')
    jst_result = ''
    if jst_star_count > 1:
        jst_result = 'A-ERROR'

    #job_sendMsg.setall(datetime(fyear, fmonth, fday, fhour, fmin))

    # 执行自动生成订单
    jgt_emin = emin + 2
    jgt_ehour = ehour
    if jgt_emin >= 60:
        jgt_emin = jgt_emin % 60
        jgt_ehour = jgt_ehour + 1

    job_generalOrder = my_user_cron.new(command='curl http://wx.9mu.net/'+str(
        mod_name)+'/index.php/Wap/AutoMission/index > /dev/null 2>&1')
    job_generalOrder_time = str(
        jgt_emin) + ' ' + str(jgt_ehour) + ' ' + str(eday) + ' ' + str(emonth) + ' *'
    job_generalOrder.set_comment("执行自动生成订单 - " + str(result[2]))

    f.write("\n生成订单：" + job_generalOrder_time)

    jgt_star_count = job_generalOrder_time.count('*')
    jgt_result = ''
    if jgt_star_count > 1:
        jgt_result = 'B-ERROR'

    # 结束一期
    jct_emin = emin + 4
    jct_ehour = ehour
    if jct_emin >= 60:
        jct_emin = jct_emin % 60
        jct_ehour = jct_ehour + 1

    job_closePeriod = my_user_cron.new(command='curl http://wx.9mu.net/'+str(
        mod_name)+'/index.php/Wap/AutoMission/closePeriodStatus > /dev/null 2>&1')
    job_closePeriod_time = str(
        jct_emin) + ' ' + str(jct_ehour) + ' ' + str(eday) + ' ' + str(emonth) + ' *'
    job_closePeriod.set_comment("结束一期 - " + str(result[2]))

    f.write("\n结束期数：" + job_closePeriod_time)
    f.write("\n\n\n------------------")
    f.close()

    jct_star_count = job_generalOrder_time.count('*')
    jct_result = ''
    if jct_star_count > 1:
        jct_result = 'C-ERROR'

    # 统一将三个任务进行enable
    if jst_result != 'A-ERROR' and jgt_result != 'B-ERROR' and jct_result != 'C-ERROR':
        job_sendMsg.setall(job_sendMsg_time)
        job_sendMsg.enable()

        job_generalOrder.setall(job_generalOrder_time)
        job_generalOrder.enable()
        job_closePeriod.setall(job_closePeriod_time)
        job_closePeriod.enable()

        my_user_cron.write()
    else:
        # 返回到php端，用于判断任务是否成功插入
        print('3job-error')
except:
    print("Error: unable to fetch data")

db.close()
