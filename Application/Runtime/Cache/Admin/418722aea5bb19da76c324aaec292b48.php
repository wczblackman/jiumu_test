<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <style type="text/css">
.fgroup {
	margin-left: 10px;
}
</style>
<link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />
<div class="pd-20">
	<!--a class="btn btn-success" onclick="showFood('添加<?php echo C('pom_admin_product_name');?>','<?php echo U("Admin/Order/addFood");?>','4','700',600)">添加<?php echo C('pom_admin_product_name');?></a-->
    <?php if($colFoods == null ): ?><div class="row">没有<?php echo C('pom_admin_product_name');?></div>
        <?php else: ?>
        <div class="mt-20">
        	
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="30"><?php echo C('pom_admin_product_name');?>名称</th>
                        <th width="50">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
                    <?php if(is_array($colFoods)): $k = 0; $__LIST__ = $colFoods;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$food): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td><?php echo ($food["foodname"]); ?></td>
                            <td>
	                            <span class="attr" style="display:inline;">
	                            	<input type="checkbox" class="defchs" name="" <?php if($food['defchoose'] != null): if($food['defchoose'] == 1): ?>checked="checked" value="1"<?php else: ?>value="0"<?php endif; else: ?>value="0"<?php endif; ?> disabled="disabled">默选&nbsp;&nbsp;
	                            	<input type="checkbox" class="reqchs" name="" <?php if($food['reqchoose'] != null): if($food['reqchoose'] == 1): ?>checked="checked" value="1"<?php else: ?>value="0"<?php endif; else: ?>value="0"<?php endif; ?> disabled="disabled">必选&nbsp;&nbsp;
	                            	￥<input type="text" name="" <?php if($food['price'] != null): ?>value="<?php echo ($food['price']); ?>"<?php else: ?>value=""<?php endif; ?> class="price" <?php if($food['price'] == null): endif; ?> style="width:60px;" disabled="disabled">
	                            </span>
                            	<a class="btn btn-success" href='<?php echo U("Admin/Order/delColFood", array("id" => $food["id"]));?>'>删除</a>
                            	<span class="fgroup">|&nbsp;&nbsp;分组:
                            	<input type="text" name="" value="<?php echo ($food['fgroup']); ?>" class="price"  style="width:60px;"></span>
                            	<a class="btn btn-success" onclick='editFGroup(this, <?php echo ($food["id"]); ?>)'>确定分组</a>
                            	</td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function showFood(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

function change(obj) {
	window.location.href="/index.php?g=Admin&m=Order&a=showAll&state="+$(obj).val();
}
function editFGroup(obj, id, pc_colid) {
	var fgid = $(obj).prev().find(".price").val();
	console.log("fgid = " + fgid);
	
	$.ajax({
		url: "<?php echo U('Admin/Order/editFGroup');?>",
		data: {
			id: id, fgid:fgid,
		},
		type: 'post',
		dataType: 'json',
		success: function(data) {
			if(data == 0) {
				layer.msg('菜品加入分组出错，请检查！', {icon: 1});
			} else {
				layer.msg('菜品加入分组成功', {icon: 1});
			}
		},
		error: function() {
			layer.msg('更新出错，请检查！', {icon: 1});
		}
	});
}
</script>

</body>

</html>