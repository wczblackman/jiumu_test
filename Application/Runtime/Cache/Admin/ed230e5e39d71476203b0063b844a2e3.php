<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />
<style type="text/css">
.text-c td img {
	width: 48px;
	height:48px;
}</style>
<div class="pd-20">
    <?php if($users == null ): ?><div class="row">没有用户</div>
        <?php else: ?>
        <div class="mt-20">
        	
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="30">昵称</th>
                        <th width="20">菜篮子</th>
                        <th width="20">套餐金额</th>
                        <th width="50">头像</th>
                        <th width="50">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(is_array($users)): $k = 0; $__LIST__ = $users;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$user): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td><?php echo ($user["nickname"]); ?></td>
                            <td><?php echo ($user["basketid"]); ?></td>
                            <td><?php echo ($user["discount_money"]); ?></td>
                            <td class="headimg"><img src="<?php echo ($user["headimgurl"]); ?>"></td>
                            <td>
                            	<a class="btn btn-success" href='<?php echo U("Admin/Buyer/delGrpUser", array("basketid" => $user["basketid"], "gid" => $gid));?>'>删除</a>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function showFood(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

function change(obj) {
	window.location.href="/index.php?g=Admin&m=Order&a=showAll&state="+$(obj).val();
}

</script>

</body>

</html>