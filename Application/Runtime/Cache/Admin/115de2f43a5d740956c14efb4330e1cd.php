<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <style type="text/css">
.req {
	color:#c73e3e;
}
.def {
	color:#5eb95e;
}
</style>
<div class="pd-20">
	<!--a class="btn btn-success" href='<?php echo U("Admin/Order/addOptionPage");?>'>添加备选</a-->
	<!--
	查看套餐：
        	<select onchange="change(this);" id="state-sel">
	        	<option value="1">进行中套餐</option>
	        	<option value="2">历史套餐	</option>
        	</select>
        	-->
        <!-- 注意是$options的第一个元素 -->
		<?php if($options[0] == null): ?><a class="btn btn-success" href='<?php echo U("Admin/Order/addOptionFood");?>'>添加备选</a>
        <?php else: ?>
        <a class="btn btn-success" href='<?php echo U("Admin/Order/asyncColFood");?>'>同步套餐菜品</a>
        <div class="mt-20">
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="400">菜品</th>
                        <th width="50">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
                	<!--bbbb-->
                    <?php if(is_array($options)): $k = 0; $__LIST__ = $options;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td>
	                        <?php if(is_array($vo)): $k1 = 0; $__LIST__ = $vo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($k1 % 2 );++$k1;?><span><?php echo ($item["fname"]); ?></span>*<span style="color:red;"><?php echo ($item["stock"]); ?></span>
	                        	<?php if($k1 < count($vo)): ?>|&nbsp;<?php endif; endforeach; endif; else: echo "" ;endif; ?>    
                            </td>
                            <td>
	                        <a class="btn btn-success" href='<?php echo U("Admin/Order/showOptionFood");?>'>查看备选</a>&nbsp;
                            <a class="btn btn-success" href='<?php echo U("Admin/Order/addOptionFood");?>'>修改备选</a>&nbsp;
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
var err = '<?php echo ($err); ?>';
function showFood(title, url, id, w, h) {
    layer_show(title, url, w, h);
}
//用户-编辑
function manage_edit(title, url, id, w, h, pc) {
	var height = 400 + (h-3)*31;
	if(pc == null) {
		height -= 31;
	}
    layer_show(title, url, w, height);
}

function change(obj) {
	window.location.href = "<?php echo U('Admin/Order/showCollocation');?>"+'/state/'+$(obj).val();
}

$(document).ready(function(){
	if(err != '') {
		layer.msg('请先准备新的一期,再配置套餐!', {
	        time: 5000,
	        btn: ['知道了'],
        });
	}
	var state = 1;
	$("option").each(function(){
		var opt_val = $(this).val(); 
		if(opt_val == state) {
			$(this).attr("selected",'');
		}
	});
});
</script>

</body>

</html>