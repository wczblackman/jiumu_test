<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />
<style type="text/css">
.peicai-btn {
	background-color: #2bb6e8;
	border-color: #2bb6e8;
}
.peicai-btn:hover {
	background-color: #2bb6f9;
	border-color: #2bb6f9;
}
.btn-cancel-order {
	margin-top: 2px;
}
</style>
<div class="pd-20">
	订单类型：
        	<select onchange="change(this);" id="state-sel">
	        	<option value="0">当期订单</option>	
	        	<option value="1">历史订单</option>
	        	<!--option value="2">已配送</option>
	        	<option value="3">已送达</option-->
        	</select>&nbsp;&nbsp;<a class="btn btn-success" href="<?php echo U('Admin/Order/curOrderDeliverList');?>">导出当期配送单</a>
        	<a class="btn btn-success" href="<?php echo U('Admin/Order/curFoodCountList');?>">导出当期菜品统计</a>
    <?php if($res == null ): ?><div class="row">没有订单</div>
        <?php else: ?>
        <div class="mt-20">
        	
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="30">订单号</th>
                        <th width="20">期数</th>
                        <th width="30">菜篮号</th>
                        <th width="100">创建时间</th>
                        <th width="50">订单状态</th>
                        <th width="30">订单类型</th>
                        <th width="150">操作</th>
                        <!--th width="50">操作</th-->
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
                    <?php if(is_array($res)): $k = 0; $__LIST__ = $res;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td><?php echo ($vo["oid"]); ?></td>
                            <td><?php echo ($pname); ?></td>
                            <td><?php echo ($vo["basketid"]); ?></td>
                            <td><?php echo (date("y-m-d H:i:s",$vo["addtime"])); ?></td>
                            <td><?php if($vo["state"] == 1): ?>未配送<?php elseif($vo["state"] == 2): ?>已配送<?php elseif($vo["state"] == 3): ?>已送达<?php endif; ?></td>
                            <td><?php if($vo["autoorder"] == 0): ?>手动<?php elseif($vo["autoorder"] == 1): ?>自动<?php elseif($vo["autoorder"] == 2): ?>取消<?php endif; ?></td>
                            <td><a class="btn btn-success" onclick="manage_edit('详情','<?php echo U("Admin/Order/detailOrder", array("oid" =>$vo["oid"]));?>','4','','<?php echo (count($vo["ofoods"])); ?>','<?php echo ($vo["postcode"]); ?>')">详情</a>							
							<?php if($vo["autoorder"] == 1 OR $vo["autoorder"] == 2): ?><a class="btn btn-success btn-cancel-order" href="<?php echo U('Admin/Order/cancelManuOrder', array('oid' => $vo['oid']));?>">取消订单</a><?php endif; ?>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function manage_edit(title, url, id, w, h, pc) {
	var height = 400 + (h-1)*31;
	if(pc == null) {
		height -= 31;
	}
    layer_show(title, url, w, height);
}

function manage_edit_order(title, url, id, w, h) {
	layer_show(title, url, w, h);
}

function change(obj) {
	//window.location.href="/index.php/Admin/Order/showAll/state/"+$(obj).val();
	window.location.href = "<?php echo U('Admin/Order/showAll', array(),false);?>"+'/state/'+$(obj).val();
}

$(document).ready(function(){
	var state = <?php echo ($state); ?>;
	$("option").each(function(){
		var opt_val = $(this).val(); 
		if(opt_val == state) {
			$(this).attr("selected",'');
		}
	});
});
</script>

</body>

</html>