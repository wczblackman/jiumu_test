<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />
<div class="pd-20">
	<a class="btn btn-success" onclick="showUserGroup('新增用户组','<?php echo U("Admin/Buyer/addUserGroup");?>','4','500',300)">添加用户组</a>
    <?php if($groups == null ): ?><div class="row">没有期数</div>
        <?php else: ?>
        <div class="mt-20">
        	
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="50">用户组名</th>
                        <th width="80">创建时间</th>
                        <th width="80">排序</th>
                        <th width="50">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
                    <?php if(is_array($groups)): $k = 0; $__LIST__ = $groups;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$group): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td><?php echo ($group["grpname"]); ?></td>
                            <td><?php echo (date("Y-m-d H:i:s", $group["create_time"])); ?></td>
                            <td><?php echo ($group["sortid"]); ?></td>
                            <td>
	                            <a class="btn btn-success" href="<?php echo U('Admin/Buyer/showGrpUsers', array('gid' => $group['id']));?>">查看用户</a>
                            	<a class="btn btn-success" onclick="showUserGroup('修改期数','<?php echo U("Admin/Buyer/addUserGroup", array("gid" =>$group["id"]));?>','4','500',350)">修改</a>
                            	<?php if($group['is_use'] == 0): ?><a class="btn btn-success" href="<?php echo U('Admin/Buyer/useGroup', array('gid' => $group['id']));?>">启用</a>
	                            <?php else: ?>
	                            <a class="btn btn-success" style="background-color: #ff1cdd;border-color: #ff1cdd;" href="<?php echo U('Admin/Buyer/useGroup', array('gid' => $group['id']));?>">启用中</a><?php endif; ?>
                            	<a class="btn btn-success" href="<?php echo U('Admin/Buyer/addUser2Grp', array('gid' => $group['id']));?>">添加用户</a>
                            	<!-- 如果当前有一期在使用中（准备|进行）的，那就只能开启 -->
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function showUserGroup(title, url, id, w, h) {
    layer_show(title, url, w, h);
}
/*
$(document).ready(function(){
	var state = <?php echo ($state); ?>;
	$("option").each(function(){
		var opt_val = $(this).val(); 
		console.log('opt_val = ' + opt_val);
		if(opt_val == state) {
			console.log('opt_val = ' + opt_val + ' 被选中 ');
			$(this).attr("selected",'');
		}
	});
});
*/
</script>

</body>

</html>