<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <style type="text/css">
.req {
	color:#c73e3e;
}
.def {
	color:#5eb95e;
}
</style>
<div class="pd-20">
	<a class="btn btn-success" href='<?php echo U("Admin/Order/addCollocationPage");?>'>添加套餐</a>
    <?php if($collocations == null): ?><div class="row">
        <?php if($state == 1): ?>当前没有套餐，请添加！<?php endif; ?>
        </div>
        <?php else: ?>
        <div class="mt-20">
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="30">套餐名称</th>
                        <th width="20">套餐价格</th>
                        <th width="250">菜品&nbsp;&nbsp;(<span style="color:#5eb95e;">绿色</span>:<span style="color:black;">默选</span>|<span style="color:#c73e3e;">红色</span>:<span style="color:black;">必选</span>)</th>
                        <th width="100">创建时间</th>
                        <th width="150">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
                	<!--bbbb-->
                    <?php if(is_array($collocations)): $k = 0; $__LIST__ = $collocations;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td><?php echo ($vo["colname"]); ?></td>
                            <td><?php echo ($vo["colmoney"]); ?></td>
                            <td>
	                        <?php if(is_array($vo['collocation_items'])): $k1 = 0; $__LIST__ = $vo['collocation_items'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($k1 % 2 );++$k1;?><span <?php if($item["reqchoose"] == 1): ?>class="req"<?php else: if($item["defchoose"] == 1): ?>class="def"<?php endif; endif; ?>><?php echo ($item["foodname"]); ?></span>*<?php echo ($item["price"]); ?>
	                        	<?php if($k1 < count($vo['collocation_items'])): ?>|&nbsp;<?php endif; endforeach; endif; else: echo "" ;endif; ?>    
                            </td>
                            <td><?php echo (date("Y-m-d H:i:s",$vo["createtime"])); ?></td>
                            <td>
                            <a class="btn btn-success" onclick="manage_edit1('详情','<?php echo U("Admin/Order/addCollocation", array("cid" =>$vo["pcolid"]));?>','4','','<?php echo (count($vo["collocation_items"])); ?>','')" href='<?php echo U("Admin/Order/addCollocationPage", array("cid" =>$vo["pcolid"]));?>'>修改套餐</a>&nbsp;
                            <a class="btn btn-success" href='<?php echo U("Admin/Order/showColFood", array("cid" =>$vo["pcolid"]));?>'>查看菜品</a>&nbsp;
                            <a class="btn btn-success" href='<?php echo U("Admin/Order/addColFood", array("cid" =>$vo["pcolid"]));?>'>添加菜品</a>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
var err = '<?php echo ($err); ?>';
function showFood(title, url, id, w, h) {
    layer_show(title, url, w, h);
}
//用户-编辑
function manage_edit(title, url, id, w, h, pc) {
	var height = 400 + (h-3)*31;
	if(pc == null) {
		height -= 31;
	}
    layer_show(title, url, w, height);
}

function change(obj) {
	window.location.href = "<?php echo U('Admin/Order/showCollocation');?>"+'/state/'+$(obj).val();
}

$(document).ready(function(){
	if(err != '') {
		layer.msg('请先准备新的一期,再配置套餐!', {
	        time: 5000,
	        btn: ['知道了'],
        });
	}
	var state = 1;
	$("option").each(function(){
		var opt_val = $(this).val(); 
		if(opt_val == state) {
			$(this).attr("selected",'');
		}
	});
});
</script>

</body>

</html>