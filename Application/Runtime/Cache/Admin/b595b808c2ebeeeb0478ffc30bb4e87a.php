<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title><?php echo C('pom_admin_title');?></title>
    <link rel="bookmark" href="/9mu/favicon.ico" />
    <link rel="icon" href="/9mu/favicon.ico" />
    <link rel="shortcut Icon" href="/9mu/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
</head>

<body id="parent">
    <header class="header cl">
        <a class="logo l" title="">
            <img src="/9mu_test/Public/images/logo.png" />
        </a>
        <!--a class="logo-t l" title="">       
        </a-->
        <a class="logo-m l" href="/" title=""></a>
        <nav class="mainnav cl" id="nav">
        </nav>
        <ul class="userbar">
            <li>超级管理员</li>
            <li class="dropDown dropDown_hover"><a href="#" class="dropDown_A"><?php echo ($admin); ?> <i class="icon-angle-down"></i></a>
                <ul class="dropDown-menu radius box-shadow">
                    <!--li><a _href="<?php echo U('System/pwdChange');?>" href="javascript:void(0)" target="main">修改密码</a></li-->
                    <li><a href="<?php echo U('Auth/logout');?>">退出</a></li>
                </ul>
            </li>
            <li><a href="<?php echo U('Auth/logout');?>" title="退出"><i class="icon-off"></i>&nbsp;退出</a></li>
        </ul>
        <a aria-hidden="false" class="nav-toggle" href="#"></a>
    </header>
    <aside class="aside">
        <input runat="server" id="divScrollValue" type="hidden" value="" />
        <div class="menu-dropdown bk_2" id="main-menu">
            <dl id="leftmenu01">
                <dt><a class="on" _href="<?php echo U('Index/welcome');?>" href="javascript:void(0)" target="main"><i class="icon-home menu-icon"></i> 系统首页</a></dt>
                <dd>
                </dd>
            </dl>
			<dl id="leftmenu01">
                <dt>
                	<a>
	                	<i class="icon-home menu-icon"></i> 用户管理
	                	<i class="icon-angle-down menu-dropdown-arrow"></i></a></dt>
                <dd>
                	<ul>
                        <li><a _href="<?php echo U('Admin/Buyer/showUsers');?>" href="javascript:void(0)" target="main">用户列表</a></li>
                        <li><a _href="<?php echo U('Admin/Buyer/showUserGroup');?>" href="javascript:void(0)" target="main">用户分组</a></li>
                        <li><a _href="<?php echo U('Admin/Buyer/showPushMsgStatus');?>" href="javascript:void(0)" target="main">当期推送情况</a></li>
                	</ul>
                </dd>
            </dl>
            <dl id="leftmenu02">
                <dt>
                	<a>
	                	<i class="icon-group menu-icon"></i><?php echo C('pom_admin_type_name');?>管理
	                	<i class="icon-angle-down menu-dropdown-arrow"></i></a></dt>
                <dd>
                    <ul>
                        <!--li><a _href="<?php echo U('Vip/index');?>" id="fd" href="javascript:void(0)" target="main">用户审核</a></li-->
                        <li><a _href="<?php echo U('Admin/Order/showAll');?>" href="javascript:void(0)" target="main">订单管理</a></li>
                        <li><a _href="<?php echo U('Admin/Order/showSorts');?>" href="javascript:void(0)" target="main">分类管理</a></li>
                        <li><a _href="<?php echo U('Admin/Order/showFoods');?>" href="javascript:void(0)" target="main"><?php echo C('pom_admin_product_name');?>管理</a></li>
                        <!--li><a _href="<?php echo U('Admin/Order/showCombopack');?>" href="javascript:void(0)" target="main">套餐管理</a></li-->
                        <li><a _href="<?php echo U('Admin/Order/showCollocation');?>" href="javascript:void(0)" target="main">套餐管理</a></li>             
                        <li><a _href="<?php echo U('Admin/Order/showOption');?>" href="javascript:void(0)" target="main">备选管理</a></li> 
                        <li><a _href="<?php echo U('Admin/Order/showPlus');?>" href="javascript:void(0)" target="main">另购管理</a></li>          
                        <li><a _href="<?php echo U('Admin/Order/showPeriods');?>" href="javascript:void(0)" target="main">期数管理</a></li>           
                    </ul>
                </dd>
            </dl>
            <!--dl id="leftmenu03">
                <dt><a><i class="icon-suitcase menu-icon"></i>报表管理<i class="icon-angle-down menu-dropdown-arrow"></i></a></dt>
                <dd>
                    <ul>
                        <li><a _href="<?php echo U('Admin/Order/');?>" href="javascript:void(0)" target="main">申请服务列表</a></li>
                        <li><a _href="<?php echo U('Business/mail');?>" href="javascript:void(0)" target="main">邮件内容设置</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="leftmenu04">
                <dt><a><i class="icon-comments-alt menu-icon"></i>咨询留言<i class="icon-angle-down menu-dropdown-arrow"></i></a></dt>
                <dd>
                    <ul>
                        <li><a _href="<?php echo U('Reply/index');?>" href="javascript:void(0)" target="main">待答复</a></li>
                        <li><a _href="<?php echo U('Index/welcomer');?>" href="javascript:void(0)" target="main">所有咨询</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="leftmenu05">
                <dt><a><i class="icon-cog menu-icon"></i>系统设置<i class="icon-angle-down menu-dropdown-arrow"></i></a></dt>
                <dd>
                    <ul>
                        <li><a _href="<?php echo U('System/mail');?>" href="javascript:void(0)" target="main">邮箱配置</a></li>
                        <li><a _href="<?php echo U('System/pwdChange');?>" href="javascript:void(0)" target="main">密码修改</a></li>
                        <li><a _href="<?php echo U('System/upFile');?>" href="javascript:void(0)" target="main">上传设置</a></li>
                        <li><a _href="<?php echo U('System/mailer');?>" href="javascript:void(0)" target="main">发送邮件(测试)</a></li>
                    </ul>
                </dd>
            </dl-->
        </div>
    </aside>
    <div class="dislpayArrow">
        <a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
    </div>
    <section class="article-box">
        <div id="tabNav" class="tabNav hidden-xs">
            <nav class="breadcrumb"> 欢迎您！ <span id="top-sub-menu"></span><a class=" btn-refresh r mr-20" id="refresh" title="刷新"><i class="icon-refresh"></i></a></nav>
            <div id="iframe_box" class="article">
                <div class="show_iframe">
                    <div style="display:none" class="loading"></div>
                    <iframe id="main" name="main" scrolling="yes" frameborder="0" src="<?php echo U('Index/welcome');?>"></iframe>
                </div>
            </div>
    </section>
    <script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
    <script>
    $(function() {
        $('#refresh').click(function() {
            document.getElementById('main').contentWindow.location.reload(true);
        });
        
        $('a[target="main"]').each(function(){
	        $(this).click(function(){
		       var val = $(this).text();
		       $('.breadcrumb').text(val);
		    }); 
        });
        <?php if($hasPerioding == true): ?>layer.msg('当前进行的期数:<?php echo ($period["pname"]); ?>', {
	        time: 5000,
	        btn: ['知道了'],
        });
        <?php else: ?>
        layer.msg('当前没有进行中的期数,请配置！', {
	        time: 5000,
	        btn: ['知道了'],
        });<?php endif; ?>
    });
    
    </script>
</body>

</html>