<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />
<div class="pd-20">
    <?php if($cur_group == null ): ?><div class="row"></div>
        <?php else: ?>
        <div class="mt-20">
        	<form action="" method="POST">
                <button class="btn btn-success" style="margin-bottom: 5px;" onclick="showOpenMsg(this)" periodid="<?php echo ($periodid["periodid"]); ?>">确认开启</button>
                <p>当前期数：<?php echo ($periodid["ppname"]); ?></p>
                <input type="hidden" name="periodid" value="<?php echo ($periodid["periodid"]); ?>">
                <table class="table table-border table-bordered table-bg table-sort table-striped">
                    <thead>
                        <tr class="text-c">
                            <th width="30">用户组</th>
                            <th width="30">套餐菜品</th>
                            <th width="80">备选菜品</th>
                        </tr>
                    </thead>
                    <!--<?php echo ($vo["postcode"]); ?>-->
                    <tbody>
                        <tr class="text-c">
                            <td>
                                <?php if(is_array($cur_group)): $i = 0; $__LIST__ = $cur_group;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$g): $mod = ($i % 2 );++$i;?><span><?php echo ($g["grpname"]); ?>&nbsp;</span><?php endforeach; endif; else: echo "" ;endif; ?>
                            </td>
                            <td>
                                <?php if(is_array($cbpfoods)): $i = 0; $__LIST__ = $cbpfoods;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$food): $mod = ($i % 2 );++$i;?><p><input type="checkbox" checked name="pcolid[]" value="<?php echo ($food["pcolid"]); ?>">套餐：<?php echo ($food["colname"]); ?></p><?php endforeach; endif; else: echo "" ;endif; ?>
                            </td>
                            <td>
                                <?php if(is_array($opFoodsList)): $i = 0; $__LIST__ = $opFoodsList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$op): $mod = ($i % 2 );++$i;?><span><?php echo ($op["foodname"]); ?>&nbsp;</span><?php endforeach; endif; else: echo "" ;endif; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function showFood(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

function change(obj) {
	window.location.href="/index.php?g=Admin&m=Order&a=showAll&state="+$(obj).val();
}
</script>

</body>

</html>