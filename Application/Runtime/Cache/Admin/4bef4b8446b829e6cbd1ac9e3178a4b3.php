<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />
<div class="pd-20">
	<a class="btn btn-success" onclick="showFood('新增一期','<?php echo U("Admin/Order/addPeriod");?>','4','500',300)">添加期数</a>
    <?php if($periods == null ): ?><div class="row">没有期数</div>
        <?php else: ?>
        <div class="mt-20">
        	
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="30">期数</th>
                        <th width="30">期数名称</th>
                        <th width="80">开始时间</th>
                        <th width="80">结束时间</th>
                        <th width="40">状态</th>
                        <th width="50">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
                    <?php if(is_array($periods)): $k = 0; $__LIST__ = $periods;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$period): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td><?php echo ($period["pname"]); ?></td>
                            <td><?php echo ($period["ppname"]); ?></td>
                            <td><?php echo (date("Y-m-d H:i:s",$period["fromtime"])); ?></td>
                            <td><?php echo (date("Y-m-d H:i:s",$period["endtime"])); ?></td>
                            <td>
                            	<?php if($period["status"] == 0): ?>未开启<?php elseif($period["status"] == 1): ?>准备<?php elseif($period["status"] == 2): ?>进行中<?php elseif($period["status"] == 3): ?>已结束<?php endif; ?>
								<?php if(($period["status"]) >= "2"): if($period["dosendmsg"] == 1): ?>| 已群发<?php else: ?>| 未群发<?php endif; endif; ?>
                            </td>
                            <td>
                            	<?php if($period["status"] == 0): ?><a class="btn btn-success" onclick="showFood('修改期数','<?php echo U("Admin/Order/addPeriod", array("periodid" =>$period["periodid"]));?>','4','500',350)">修改</a><?php endif; ?>
                            	<!-- 如果当前有一期在使用中（准备|进行）的，那就只能开启 -->
                            	<?php if($using == true): if($period["status"] == 1): ?><!-- <a class="btn btn-success" onclick="showOpenMsg(<?php echo ($period['periodid']); ?>)" style="background-color:#EA447D;border-color:#EA447D;">开启</a> -->
									<a class="btn btn-success" href="<?php echo U("Admin/Order/bindPeriods", array("periodid" =>$period["periodid"]));?>" style="background-color:#EA447D;border-color:#EA447D;">开启</a><?php endif; ?>
                            	<?php else: ?>
	                            	<?php if($period["status"] == 0): ?><a class="btn btn-success" onclick="showReadyMsg(<?php echo ($period['periodid']); ?>)">准备</a><?php endif; endif; ?>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function showFood(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

function change(obj) {
	window.location.href="/index.php?g=Admin&m=Order&a=showAll&state="+$(obj).val();
}
function showReadyMsg(pid) {
	layer.confirm('确定这期吗？确定完后去配置当期套餐、备选、另购的菜品，再开启这期！', {
		btn: ['确定', '取消']
	}, function() {
		$.ajax({
			url: "<?php echo U('Admin/Order/readyPeriod');?>",
			data: {
				periodid: pid,
			},
			type: 'post',
			dataType: 'json',
			success: function(data) {
				layer.msg(data.result, function(){
					window.location.href = '<?php echo U("Admin/Order/showPeriods");?>';
				});
			},
			error: function() {
				layer.msg('更新出错，请检查！', {icon: 1});
			}
		});
	}, function() {
		
	});
}
// function showOpenMsg(pid) {
// 	layer.confirm('请确定当期 套餐 会员已经配置完成！<br/>会员分组：<?php echo ($cur_group['grpname']); ?><br/>套餐菜品：<?php echo ($cbpfoods_str); ?><br/>备选菜品：<?php echo ($opfoods_str); ?>', {
// 		btn: ['确定', '取消']
// 	}, function() {
// 		$.ajax({
// 			url: "<?php echo U('Admin/Order/openPeriod');?>",
// 			data: {
// 				periodid: pid,
// 			},
// 			type: 'post',
// 			dataType: 'json',
// 			success: function(data) {  
// 				layer.msg(data.result, function(){
// 					window.location.href = '<?php echo U("Admin/Order/showPeriods");?>';
// 				});
// 			},
// 			error: function() {
// 				layer.msg('更新出错，请检查！', {icon: 1});
// 			}
// 		});
// 	}, function() {
		
// 	});
// }


</script>

</body>

</html>