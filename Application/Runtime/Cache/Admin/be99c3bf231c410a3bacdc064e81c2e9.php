<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <style type="text/css">
.text-c td img {
	width: 48px;
	height:48px;
}</style>

<link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />

<div class="pd-20">
    <?php if($pushMsgUsers == null ): ?><div class="row">没有结果</div>
        <?php else: ?>
        <div class="mt-20">
	        <div style="padding-bottom: 10px;">
				<h3 style="padding-left: 5px;"><span><?php echo ($ppname); ?></span>-页面每<span style="color:red">20</span>秒刷新一次，请注意查看消息送达结果</h3>
				</div>
	        </div>
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
						<th width="25">菜篮子号</th>
                        <th width="30">昵称</th>
                        <th width="30">真名</th>
                        <th width="50">推送结果</th>
                        <th width="100">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
	                <?php $count=0; ?>
                    <?php if(is_array($pushMsgUsers)): $k = 0; $__LIST__ = $pushMsgUsers;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$user): $mod = ($k % 2 );++$k; $count++; ?>
                        <tr class="text-c">
							<td><?php echo ($user["basketid"]); ?></td>
                            <td class="nickname"><?php echo ($user["nickname"]); ?></td>
                            <td><?php echo ($user["truename"]); ?></td>
                            <td><?php echo ($user["pushmsgresult"]); ?></td>
                            <td>
	                            <a class="btn btn-success" href="<?php echo U('Admin/Buyer/repushMsg', array('openid' => $user['openid']));?>">重新发送</a>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($Page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
// 用户编辑
function add_edit_User(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

function myrefresh(){
window.location.reload();
}
setTimeout('myrefresh()',20000); //指定1秒刷新一次
</script>

</body>

</html>