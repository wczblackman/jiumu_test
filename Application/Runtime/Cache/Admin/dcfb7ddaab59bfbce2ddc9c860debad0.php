<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="keywords" content="" /> 
<meta name="description" content="" />  
<title><?php echo C('pom_admin_title');?></title>
<link rel="bookmark" href="/favicon.ico" />
<link rel="shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
<script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
<script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
<![endif]-->
<link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
<link href="/9mu_test/Public/css/login.css" rel="stylesheet" type="text/css" />
<link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
<!--[if IE 7]>
<link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
<![endif]-->
</head>
<body>
<div class="loginWraper">
  <div id="loginform" class="loginBox">
  	<div class="loginBoxTitle">
		    <img src="/9mu_test/Public/images/logo.png" />&nbsp;<?php echo C('pom_admin_title');?>
    </div>
    <form class="form form-horizontal" method="post">
      <div class="row cl">
        <label class="form-label col-3"><i class="icon-user"></i></label>
        <div class="formControls col-8">
          <input id="user" name="user" type="text" placeholder="帐户" class="input-text size-M">
        </div>
      </div>
      <div class="row cl">
        <label class="form-label col-3"><i class="icon-lock"></i></label>
        <div class="formControls col-8">
          <input id="password" name="password" type="password" placeholder="密码" class="input-text size-M">
        </div>
      </div>
      <div class="row cl">
        <div class="formControls col-8 col-offset-3">
          <input class="input-text size-M" name="code" id="code" type="text" placeholder="验证码"  style="width:150px;">
          <img id="codeimg" src='<?php echo U("Admin/Auth/verify",array(),false);?>' /> <!--a id="kanbuq">看不清，换一张</a--> </div>
      </div>
      <div class="row mb-20">
        <div class="formControls col-8 col-offset-3">
          <input name="" type="button" id="submit" class="btn btn-admin size-M" value="登录">
          <input name="" type="reset" class="btn btn-default size-M ml-20" value="取消">
        </div>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script> 
<script type="text/javascript">

$(function(){

  //判断是否是在顶级窗口中打开
  if (window.top !== window.self) {
    window.top.location.reload();
  }
  
  $('#codeimg').click(function(){
    $(this).attr("src","<?php echo U('Auth/verify', array(), false);?>");
  });

  $('#submit').click(function() {   
    var user = $("#user").val();
    var password = $("#password").val();
    var code = $("#code").val();
    if (user == '') {
        alert("请输入完整");
        return false;
    }
    if (password == '') {
        alert("请输入完整");
        return false;
    }  
    if (code == '') {
        alert("请输入完整");
        return false;
    }
    $.ajax({
          type:"post",
          url:"<?php echo U('Admin/Auth/login', array(), false);?>",
          data:$('form').serialize(),
          dataType: "json", 
          success : function(res){
            if(res['status'] == 0){
                alert(res['info']);
                return;
            }
            else if(res['status'] == 1){
              window.location.replace("<?php echo U('Admin/Index/index', array(), false);?>");
            }else{
              return;
            }

          },
          error : function(res) {  
            //console.trace(res);
          }   
     });          

  });

});
</script>
</body>
</html>