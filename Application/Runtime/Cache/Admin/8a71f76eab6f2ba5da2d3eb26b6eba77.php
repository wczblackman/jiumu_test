<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <div class="pd-20">
    <form method="post" class="form form-horizontal" action="<?php echo U('Admin/Buyer/bandUser');?>" onsubmit="return bandTip()">
    	<input type="hidden" name="id" value="<?php echo ($user['id']); ?>">
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>昵称</label>
            <div class="col-6">
	            <input type="text" class="input-text" value="<?php echo ($user['nickname']); ?>" placeholder="" id="sort_name" name="sort_name" datatype="*2-16" nullmsg="" disabled="disabled">
            </div>
            <div class="col-3"><img src="<?php echo ($user['headimgurl']); ?>" width="50px" height="50px" style="padding-left: 20px;"></div>
        </div>
        <!--div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>套餐</label>
            <div class="col-6">
	            <input type="text" class="input-text" <?php if($user["colid"] == NULL): ?>value=""<?php else: ?>value="<?php echo ($user["colid"]); ?>"<?php endif; ?> placeholder="" id="sort_name" name="colid" datatype="*2-16" nullmsg="">
            </div>
            <div class="col-3"></div>
        </div-->
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>菜篮子号</label>
            <div class="col-6">
	            <input type="text" class="input-text" <?php if($user["basketid"] == 0): ?>value=""<?php else: ?>value="<?php echo ($user["basketid"]); ?>"<?php endif; ?> placeholder="" id="basketid" name="basketid" datatype="*2-16" nullmsg="">
            </div>
            <div class="col-3"><a class="btn btn-success" onclick="getUserInfo()" style="margin-left: 20px; font-size: 12px;padding: 0px 0px; height: 24px;">获取用户</a></div>
        </div>
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>真实姓名</label>
            <div class="col-6">
	            <input type="text" class="input-text" <?php if($user["truename"] == NULL): ?>value=""<?php else: ?>value="<?php echo ($user["truename"]); ?>"<?php endif; ?> placeholder="" id="truename" name="truename" datatype="*2-16" nullmsg="">
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>地址</label>
            <div class="col-6">
	            <input type="text" class="input-text" <?php if($user["address"] == NULL): ?>value=""<?php else: ?>value="<?php echo ($user["address"]); ?>"<?php endif; ?> placeholder="" id="address" name="address" datatype="*2-16" nullmsg="">
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>电话</label>
            <div class="col-6">
	            <input type="text" class="input-text" <?php if($user["tell"] == NULL): ?>value=""<?php else: ?>value="<?php echo ($user["tell"]); ?>"<?php endif; ?> placeholder="" id="tell" name="tell" datatype="*2-16" nullmsg="">
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row cl">
            <div class="col-9 col-offset-3">
                <input name="fomr-add" class="btn btn-success " type="submit" value="&nbsp;提交&nbsp;">
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script type="text/javascript">
function getUserInfo() {
	var bid = $('#basketid').val();
	$.ajax({
		url: "<?php echo U('Admin/Buyer/getOneUserInfo');?>",
		data: {
			basketid: bid,
		},
		type: 'post',
		dataType: 'json',
		success: function(data) {
			if(data.code == 1) {
				$('#truename').val(data.truename);
				$('#tell').val(data.tell);
				$('#address').val(data.address);
			} else if(data.code == 0) {
				layer.msg(data.result, {icon: 1});
			}
		},
		error: function() {
			layer.msg('更新出错，请检查！', {icon: 1});
		}
	});
}

function bandTip() {
	var basketid = $('#basketid').val();
	var id   = $('#id').val();
	layer.confirm('确定绑定？', {
		btn: ['确定', '取消']
	}, function() {
		return true;
	}, function() {
		return false;
	});
}
</script>
</body>

</html>