<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <div class="pd-20">
    <p class="f-20 text-success">欢迎您登录<?php echo C('pom_admin_title');?></p>
    <ul class="quick-menu">
        <a href="<?php echo U('Admin/Order/showAll');?>" target="main">
            <span class="menu-icon"><i class="icon-user"></i></span>
            <span class="badge badge-danger radius"><?php echo ($newOrderTotal); ?></span>
            <p>订单管理</p>
        </a>
        <a href="<?php echo U('Admin/Order/showSorts');?>" target="main">
            <span class="menu-icon"><i class="icon-tasks"></i></span>
            <p>分类管理</p>
        </a>
        <a href="<?php echo U('Admin/Order/showFoods');?>" target="main">
            <span class="menu-icon"> <i class="icon-comments-alt"></i></span>
            <p><?php echo C('pom_admin_product_name');?>管理</p>
        </a>
        <!--a href="<?php echo U('Admin/Order/showCombopack');?>" target="main">
            <span class="menu-icon"> <i class="icon-comments-alt"></i></span>
            <p>套餐管理</p>
        </a-->
        <a href="<?php echo U('Admin/Order/showCollocation');?>" target="main">
            <span class="menu-icon"> <i class="icon-comments-alt"></i></span>
            <p>套餐管理</p>
        </a>
        <a href="<?php echo U('Admin/Order/showOption');?>" target="main">
            <span class="menu-icon"> <i class="icon-comments-alt"></i></span>
            <p>备选管理</p>
        </a>
        <a href="<?php echo U('Admin/Order/showPlus');?>" target="main">
            <span class="menu-icon"> <i class="icon-comments-alt"></i></span>
            <p>另购管理</p>
        </a>
        <a href="<?php echo U('Admin/Order/showPeriods');?>" target="main">
            <span class="menu-icon"> <i class="icon-comments-alt"></i></span>
            <p>期数管理</p>
        </a>
        <!--a href="<?php echo U('Admin/Order/showCharts');?>" target="main">
        	<span class="menu-icon"><i class="icon-comments-alt"></i></span>
        	<p>报表管理</p>
        </a-->
        <!--a>
            <span class="menu-icon"><i class="icon-cogs"></i></span>
            <p>系统设置</p>
        </a-->
    </ul>
    <table class="table table-border table-bordered table-bg mt-20">
        <thead>
            <tr>
                <th colspan="2" scope="col">服务器信息</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>服务器域名</td>
                <td>
                    <?php echo $_SERVER['HTTP_HOST']; ?>
                </td>
            </tr>
            <tr>
                <td>服务器IP地址</td>
                <td>
                    <?php echo GetHostByName($_SERVER['SERVER_NAME']); ?>
                </td>
            </tr>
            <tr>
                <td>服务器端口 </td>
                <td>
                    <?php echo $_SERVER['SERVER_PORT']; ?>
                </td>
            </tr>
            <tr>
                <td>PHP版本号 </td>
                <td>
                    <?php echo PHP_VERSION; ?>
                </td>
            </tr>
            <tr>
                <td>MySql版本号 </td>
                <td>
                	<?php mysql_connect('127.0.0.1','root','Chint!9Mu?'); ?>
                    <?php echo mysql_get_server_info(); ?>
                </td>
            </tr>
            <tr>
                <td>服务器版本号 </td>
                <td>
                    <?php echo $_SERVER['SERVER_SOFTWARE']; ?>
                </td>
            </tr>
            <tr>
                <td>服务器操作系统 </td>
                <td>
                    <?php echo PHP_OS; ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>

</body>

</html>