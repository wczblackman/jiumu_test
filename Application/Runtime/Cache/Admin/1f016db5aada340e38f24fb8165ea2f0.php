<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <script src="/9mu_test/Public/js/artDialog/jquery.artDialog.js?skin=default"></script>
<script src="/9mu_test/Public/js/artDialog/plugins/iframeTools.js"></script>
<script src="/9mu_test/Public/js/date/WdatePicker.js"></script>
<link rel="stylesheet" type="text/css" href="/9mu_test/Public/css/datepicker.css">
<link rel="stylesheet" type="text/css" href="/9mu_test/Public/css/WdatePicker.css">
<style type="text/css">
a.a_upload,a.a_choose{
	border:1px solid #3d810c;
	box-shadow:0 1px #cccccc;
	-moz-box-shadow:0 1px #cccccc;
	-webkit-box-shadow:0 1px #cccccc;
	cursor:pointer; 
	display: inline-block;
	text-align: center;
	vertical-align:bottom;
	overflow:visible;
	border-radius:3px;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	vertical-align:middle;
	background-color: #f1f1f1;
	background-image: -webkit-linear-gradient(bottom, #ccc 0%, #e5e5e5 3%, #fff 97%, #fff 100%);
	background-image: -moz-linear-gradient(bottom, #ccc 0%, #e5e5e5 3%, #fff 97%, #fff 100%);
	background-image: -ms-linear-gradient(bottom, #ccc 0%, #e5e5e5 3%, #fff 97%, #fff 100%);
	color: #000;
	border:1px solid #aaa;
	padding:2px 8px 2px 8px;
	text-shadow: 0 1px #ffffff;
	font-size: 14px;
	line-height: 1.5;
}
</style>
<div class="pd-20">
    <form action="" method="post" class="form form-horizontal" action="<?php echo U("Admin/Order/addPeriod", array('periodid' => '1'));?>" onsubmit="return checkForm()">
    	<input type="hidden" name="periodid" value="<?php echo ($period['periodid']); ?>">
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>期数</label>
            <div class="col-6">
	            <input type="text" class="input-text" value="<?php echo ($period['pname']); ?>" placeholder="" id="pname" name="pname" datatype="*2-16" nullmsg="" <?php if($period['periodid'] != null): ?>disabled="disabled"<?php endif; ?>>
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>期数名称</label>
            <div class="col-6">
	            <input type="text" class="input-text" value="<?php echo ($period['ppname']); ?>" placeholder="*月第*期" id="ppname" name="ppname" datatype="*2-16" nullmsg="" <?php if($period['periodid'] != null): ?>disabled="disabled"<?php endif; ?>>
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>开始时间</label>
            <div class="col-6">
	            <input type="input" class="px" id="fromtime" name="fromtime" value="<?php echo ($period["fromtime"]); ?>"  placeholder="2016-01-01 01:02:03" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})">
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row cl">
        	<label class="form-label col-3"><span class="c-red"></span>结束时间</label>
            <div class="col-6">
            	<input type="input" class="px" id="endtime" name="endtime" value="<?php echo ($period["endtime"]); ?>" placeholder="2016-12-31 23:59:59" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" >
            </div>
            <div class="col-3"></div>
        </div>
        <div class="row cl">
            <div class="col-9 col-offset-3">
                <input name="fomr-add" class="btn btn-success " type="submit" value="&nbsp;提交&nbsp;">
            </div>
        </div>
    </form>
</div>
<script>
// 适配3.2
var site_url = '<?php echo U("Admin/Upyun/upload", array(), false);?>';
//用户-编辑
function manage_edit(title, url, id, w, h, pc) {
	var height = 440 + (h-1)*31;
	if(pc == null) {
		height -= 31;
	}
    layer_show(title, url, w, height);
}

function checkForm() {
	var fromtime = $('#fromtime').val();
	var endtime = $('#endtime').val();
	
	if(fromtime == '' || endtime == '') {
		alert('开始时间和结束时间不能为空！');
		return false;
	}
	
	var repfromtime = fromtime.replace(new RegExp('-',"gm"), '/');
	var fromseconds = (new Date(repfromtime)).getTime();
	
	var rependtime = endtime.replace(new RegExp('-', "gm"), '/');
	var endseconds = (new Date(rependtime)).getTime();
	
	if(isNaN(fromseconds) == true || isNaN(endseconds) == true) {
		alert('时间格式不对, 请重新检查！');
		return false;
	}
	
	if(endseconds < fromseconds) {
		alert('结束时间不能小于开始时间！');
		return false;
	}
	
	return true;
}

$(document).ready(function(){
	<?php if($food != null): ?>var sid = <?php echo ($sid); ?>;
	$("option").each(function(){
		var opt_val = $(this).val(); 
		if(opt_val == sid) {
			$(this).attr("selected",'');
		}
	});
	<?php else: ?>
	var firstSort = $('#state-sel').children().first();
	firstSort.attr("selected",'');
	var sid = firstSort.val();
	$('#sid').val(sid);<?php endif; ?>
});
</script>
</body>

</html>