<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <style type="text/css">
.text-c td img {
	width: 48px;
	height:48px;
}</style>

<link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />

<div class="pd-20">
    <?php if($users == null ): ?><div class="row">没有订单</div>
        <?php else: ?>
        <div class="mt-20">
	        <div style="padding-bottom: 10px;">
		        <form method="POST" action="<?php echo U('Admin/Buyer/showUsers');?>">
	        	<a class="btn btn-success" onclick="synchWXUser()" id="synchBtn" onmouseover="showSynchBtnTips(this)">同步微信用户</a>&nbsp;&nbsp;
	<a class="btn btn-success"  onclick="add_edit_User('新增用户组','<?php echo U("Admin/Buyer/addUser");?>','4','500',400)">添加用户</a>
				<div style="display: inline;float: right;">
				<span style="padding-left: 5px;">关键词：</span>
				<input type="text" class="input-text" id="keyword" name="keyword" style="width: 200px;" value="" placeholder="请输入关键字...">
				<input type="submit" class="btn btn-success" value="搜索"/>
				</div>
				</form>
	        </div>
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="20">编号</th>
                        <th width="30">昵称</th>
                        <th width="50">头像</th>
                        <th width="30">真名</th>
                        <th width="25">菜篮子号</th>
                        <th width="20">套餐金额</th>
                        <th width="250">地址</th>
                        <th width="50">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
	                <?php $count=0; ?>
                    <?php if(is_array($users)): $k = 0; $__LIST__ = $users;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$user): $mod = ($k % 2 );++$k; $count++; ?>
                        <tr class="text-c">
                            <td><?php echo ($user["id"]); ?></td>
                            <td class="nickname"><?php echo ($user["nickname"]); ?></td>
                            <td class="headimg"><img src="<?php echo ($user["headimgurl"]); ?>"></td>
                            <td><?php echo ($user["truename"]); ?></td>
                            <td><?php echo ($user["basketid"]); ?></td>
                            <td><?php echo ($user["discount_money"]); ?></td>
                            <td><?php echo ($user["address"]); ?></td>
                            <td>
                            	<a class="btn btn-success" id="btn-tips-<?php echo ($count); ?>" onclick="add_edit_User('修改用户', '<?php echo U("Admin/Buyer/editUser", array("id" => $user["id"]));?>', '4','500',420)" onmouseover="showBndBtnTips(this)">绑定用户</a>
                            	<a class="btn btn-success" onclick="update_user(this,'<?php echo ($user["openid"]); ?>')">更新用户</a>
                            </td>
                            <!--td><a class="btn btn-success" onclick="manage_edit('用户修改','<?php echo U(" Vip1/userEdit ",array("openid "=>$vo["openid "]));?>','4','','450', 'aaa')">修改</a></td-->
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($Page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
// 用户编辑
function add_edit_User(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

// 更新用户微信信息
function update_user(id, openid) {
	$.ajax({
		url: '<?php echo U("Admin/Buyer/updateUserInfo");?>',
		type: 'POST',
		asyn:true,
		data:{
			id:openid
		},
		timeout: 10000,
		dataType:'json',
		success:function(data, textStatus, jsqXHR) {
			if(data['headimgurl'] == null) {
				alert(data);
			}else {
				var collocations = eval(data);
				var parent = $(id).parent().parent();
				parent.children('.nickname').text(data['nickname']);
				parent.children('.headimg').children('img').attr('src', data['headimgurl']);
			}
		},
		error:function(data, textStatus) {
			alert("获取用户资料出错或用户已经取消关注");
		},
	});
}

function showBndBtnTips(btn) {
	var id = $(btn).attr('id');
	layer.tips('将微信用户与签约用户绑定', '#'+id, {
	  tips: [4, '#005AAB']
	});
}

function showSynchBtnTips(btn) {
	var id = $(btn).attr('id');
	
	layer.tips('用户多，更新较慢<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;请稍等!', '#'+id, {
	  tips: [3, '#005AAB']
	});
}

function synchWXUser() {
	var index = layer.load(0);
	$.ajax({
		url: '<?php echo U("Admin/Buyer/synchWXUser");?>',
		type: 'POST',
		asyn: true,
		data: {},
		timeout: 100000,
		dataType: 'json',
		success: function(data, textStatus, jsqXHR) {
			if(data.code == 0) {
				layer.close(index); 	// 关闭加载tips
				window.location.href='<?php echo U("Admin/Buyer/showUsers");?>';
			}
		}
	});
}

$(document).ready(function(){
	// 处理分页链接，加入keyword，注意和thinkphp url模式适配
	$(".pager").find(".num").each(function(){
		var url = $(this).attr("href");
		if(url.indexOf("/keyword") == -1) {
			url = url + '/keyword/' + '<?php echo ($keyword); ?>';
			$(this).attr("href", url);
		}
	});
});
</script>

</body>

</html>