<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <div class="pd-20">
	<a class="btn btn-success" onclick="showSort('添加分类','<?php echo U("Admin/Order/addSort");?>','4','',450)">添加分类</a>
    <?php if($sorts == null ): ?><div class="row">没有订单，请添加分类</div>
        <?php else: ?>
        <div class="mt-20">
        	
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="30">分类名称</th>
                        <th width="250">分类介绍</th>
                        <th width="80"><?php echo C('pom_admin_product_name');?>个数</th>
                        <th width="50">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(is_array($sorts)): $k = 0; $__LIST__ = $sorts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sort): $mod = ($k % 2 );++$k;?><tr class="text-c">
                            <td><?php echo ($sort["name"]); ?></td>
                            <td><?php echo ($sort["des"]); ?></td>
                            <td><?php echo ($sort["num"]); ?></td>
                            <td>
                            	<a class="btn btn-success" onclick="showSort('分类修改','<?php echo U("Admin/Order/addSort",array("sid"=>$sort["id"]));?>','4','',450)">修改</a>
                            	<a class="btn btn-success" onclick="showMsg(<?php echo ($sort['id']); ?>)">删除</a>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper">
        <?php echo ($page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function showSort(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

function showMsg(sid) {
	var res = confirm("将删除此分类下所有的<?php echo C('pom_admin_product_name');?>！");
	if(res == true) {
		window.location.href = '<?php echo U("Admin/Order/delSort");?>'+sid;
	} 
}

</script>

</body>

</html>