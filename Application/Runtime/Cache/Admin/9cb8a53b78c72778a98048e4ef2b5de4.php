<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>VIP</title>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/9mu_test/Public/js/html5.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/respond.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/PIE_IE678.js"></script>
    <![endif]-->
    <link href="/9mu_test/Public/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="/9mu_test/Public/css/font-awesome.min.css" rel="stylesheet">
    <link href="/9mu_test/Public/lib/icheck/icheck.css" rel="stylesheet" />
    <link href="/9mu_test/Public/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/9mu_test/Public/css/font-awesome-ie7.min.css">
    <![endif]-->
    <script type="text/javascript" src="/9mu_test/Public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/core.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/js/admin.js"></script>
    <script type="text/javascript" src="/9mu_test/Public/lib/icheck/jquery.icheck.min.js"></script>
</head>

<body>
    <link href="/9mu_test/Public/css/page.css" rel="stylesheet"  type="text/css" />
<div class="pd-20">
	<form method="POST" action="<?php echo U('Admin/Buyer/addUser2Grp', array('gid'=> $gid));?>">
		<span>关键词：</span>
		<input type="text" class="input-text" id="keyword" name="keyword" style="width: 200px;" value="" placeholder="请输入关键字...">
		<input type="submit" class="btn btn-success" value="搜索"/>
	</form>
    <?php if($users == null ): ?><div class="row">没有用户</div>
        <?php else: ?>
        <div class="mt-20">
        	
            <table class="table table-border table-bordered table-bg table-sort table-striped">
                <thead>
                    <tr class="text-c">
                        <th width="80">昵称</th>
                        <th width="80">真名</th>
                        <th width="80">菜篮子</th>
                        <th width="auto">操作</th>
                    </tr>
                </thead>
                <!--<?php echo ($vo["postcode"]); ?>-->
                <tbody>
                    <?php if(is_array($users)): $k = 0; $__LIST__ = $users;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$user): $mod = ($k % 2 );++$k;?><tr class="text-c" data-openid="<?php echo ($user['aopenid']); ?>" data-basketid="<?php echo ($user['abasketid']); ?>">
                            <td><?php echo ($user["anickname"]); ?></td>
                            <td><?php echo ($user["atruename"]); ?></td>
                            <td><?php echo ($user["abasketid"]); ?></td>
                            <td>
                            	<a class="btn btn-success del_user" href="javascript:void(0);" <?php if($user["cgid"] != $gid): ?>style="display:none;"<?php else: ?>style="display:inline;"<?php endif; ?>>删除</a>
                            	<a class="btn btn-success add_user" href="javascript:void(0);" <?php if($user["cgid"] != $gid): ?>style="display:inline;"<?php else: ?>style="display:none;"<?php endif; ?>>加入</a>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div><?php endif; ?>
    <div class="row cl dataTables_wrapper" id="upage">
        <?php echo ($Page); ?>
    </div>
</div>
<script type="text/javascript" src="/9mu_test/Public/lib/layer/layer.js"></script>
<script>
//用户-编辑
function showFood(title, url, id, w, h) {
    layer_show(title, url, w, h);
}

$(document).ready(function(){
	// 处理分页链接，加入keyword，注意和thinkphp url模式适配
	$(".pager").find(".num").each(function(){
		var url = $(this).attr("href");
		if(url.indexOf("/keyword") == -1) {
			url = url + '/keyword/' + '<?php echo ($keyword); ?>';
			$(this).attr("href", url);
		}
	});
	
	// 从套餐中删除菜品
	$(".del_user").each(function(){
		$(this).click(function(){
			var that = $(this);
			var openid = $(this).parents(".text-c").attr("data-openid");
			var basketid = $(this).parents(".text-c").attr("data-basketid");
			$.ajax({
				url: "<?php echo U('Admin/Buyer/del_user2grp', array('gid'=> $gid));?>",
				data: {
					openid: openid,
					basketid: basketid,
				},
				type: 'post',
				dataType: 'json',
				success: function(data) {
					that.css("display", "none");
					that.next().css("display", "inline");
					alert(data.result);
				},
				error: function() {
					alert("删除失败");
				}
			});
		});
	});
	
	// 往套餐中添加菜品
	$(".add_user").each(function(){
		$(this).click(function(){
			var that = $(this);
			var openid 	 = $(this).parents(".text-c").attr("data-openid");
			var basketid = $(this).parents(".text-c").attr("data-basketid");
			var attrEl = $(this).parent();
			$.ajax({
				url: "<?php echo U('Admin/Buyer/add_user2grp', array('gid'=> $gid));?>",
				data: {
					openid: openid,
					basketid: basketid,
				},
				type: 'post',
				dataType: 'json',
				success: function(data) {
					if(data.code==1){
						that.css("display", "none");
						that.prev().css("display", "inline");
					}
					alert(data.result);
				},
				error: function() {
					alert("添加失败");
				}
			});
		});
	});
});

</script>

</body>

</html>