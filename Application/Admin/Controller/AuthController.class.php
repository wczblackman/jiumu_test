<?php 
namespace Admin\Controller;
use Think\Controller;
/**
 * 认证类
 */
class AuthController extends Controller{

	protected function _initialize(){
		layout(false);//登录页面没有布局
	}

	//登录
	public function login(){
		if($_POST){
			/* tp 3.2 */
			$postcode = I('post.code');
			$verify = new \Think\Verify();
			\Think\Log::write("post.code =>".$postcode);
			if(!$verify->check($postcode, '')) {
				$data = array();
				$data['status'] = 0;
				$data['info'] = '验证码错误';
				$this->ajaxReturn($data);
				exit;
			}
			/* tp 3.1*/
			/*
            if(md5(I('post.code')) != I('session.verify')){
                $this->ajaxReturn(0,"验证码错误",0);
                exit;
            }*/
            if(!$this->loginTo(I('post.user'), I('post.password'))){
	        	\Think\Log::write("username is wrong");
	        	$data = array();
	        	$data['status'] = 0;
	        	$data['info'] = "用户名或密码有误";
                $this->ajaxReturn($data);
                exit;
	        }else{
	        	\Think\Log::write("user password is ok!");
	        	$data = array();
	        	$data['status'] = 1;
	        	
	        	$this->ajaxReturn($data);
	        	exit;
	        }
        }           
		else{			
			$md5SeKey = md5('THINKPHP.CN');
			$this->assign('md5SeKey', $md5SeKey);
			$this->display();
		}

	}
	/**
	 * 应用登出
	 */
	public function logout(){
		session(null);
		\Think\Log::write("logout -> GROup___Name");
		$this->redirect('Admin/Auth/login');
	}

	/**
	 * 尝试以某账号登录
	 * @return bool  登录成功返回true， 反之false
	 */
	public function loginTo($user, $password){
		// 超级管理员登录 
		if( $user == C('super_user') AND $password == C('super_password') ){
			//define('IS_ROOT', true);

			$admin = array(
				'user'   =>  C('super_user'),
				'password'  =>  C('super_password')
			);
			\Think\Log::write("enter to loginAccount function =>".dump($admin, false));
			loginAccount($admin);
			return $admin;
		}
	}

	public function verify() {
		$config = [
             'fontSize' => 19, // 验证码字体大小
             'length' => 4, // 验证码位数
             'imageH' => 34
        ];
        $verify = new \Think\Verify($config);
        $verify->entry();
	}

	/* tp 3.1 */
	public function verify_3_1(){
		ob_clean();
	    
	    \Think\Image::buildImageVerify();
	}

	public function test(){
		$business_model = M("business_category",'crm_cs_','db_config');
		$where = array("id"=>1);
		$res= $business_model->where($where)->find();
		$path = C('DOWNLOAD_PATH').'/'.$res['save_name'];
		$email = 'gs1357@163.com';
		$sendinfo = sendMail($email, $path, $res['file_name']);
	 }

	 public function ct(){
	 	echo business_cn(1);
	 	exit;
	 }
	 public function mtest(){

	 		$cid = 3;
	 		$email = 'gs1357@163.com';
	 	    $business_c_m = M('business_category','crm_cs_','db_config');
            //$where = array("id"=>$cid);
            $res= $business_c_m->where(array('id'=>$cid))->find();
            $mail_subject = $res['mail_subject'];
            $mail_message = $res['mail_message'];

            $business_f_v_m = M('business_files_v','crm_cs_','db_config');
            $file_res = $business_f_v_m->where(array('bid' => $cid ))->field('file_name,file_path,show_name')->select();

            $send_info = send_mail($email,$mail_subject,$mail_message,attach_arr($file_res));

            exit();
            //$sendinfo = sendMail($email, $path, $res['file_name']);
            //???是否同时需要添加发送到管理人员提示?????
            if($send_info === true){
                $this->assign("res",1);
            }
            else{//邮件发送失败时
                $this->assign("res",0);
            }


            //$path = C('DOWNLOAD_PATH').'/'.$res['save_name'];
            //$send_info = send_mail($email,$mail_subject,$mail_message,$file_res);

            //$sendinfo = sendMail($email, $path, $res['file_name']);
            //???是否同时需要添加发送到管理人员提示?????
            //if($send_info === true){
                //$this->assign("res",1);
            //}
            //else{//邮件发送失败时
            //    $this->assign("res",0);
            //}
	 }

	 public function md5(){
	 	$yzm = array('V', 'Q', 'd', '6');
	 	$yzm1 = implode('', $yzm);
	 	\Think\Log::write("验证码1 = " . $yzm1);

	 	$yzm2 = strtoupper($yzm1);
	 	\Think\Log::write("验证码2 = " . $yzm2);

	 	$key = substr(md5('THINKPHP.CN'), 5, 8);
	 	$str = substr(md5($yzm2), 8, 10);
        $yzm3 = md5($key . $str);

	 	\Think\Log::write("验证码3 = " . $yzm3);

	 	$str = I('md5s');
	 	echo "md5(".$str.") = " . md5($str);
	 }
}


