<?php 
namespace Admin\Controller;
use Think\Controller;

/**
 * Admin模块公共控制器
 */

 class AdminController extends Controller{
	protected function _initialize(){
		if(session('authName')==C('super_user')){ 
			$this->assign('admin',session('authName')); 
		}else{ 
//			if ( __SELF__ != '/' ) { 
//                $this->display('/Auth/login');;
//			}else{ //当前在首页面
			$path = U('Admin/Auth/login',null,false);
			\Think\Log::write('Location:' . $path);
			header("Location:" . $path);	
			exit();		 
		}
	}


	private function _checkLogin(){
		if( session('authName') ){
			session('authName')==C('super_user')? define('IS_ROOT', true) : NULL;
			return true;
		}else{ 
			return false;

		}
	}


 }

