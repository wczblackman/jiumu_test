<?php
namespace Admin\Controller;
use Admin\Controller\AdminController;

class IndexController extends AdminController {

	public function _initialize(){
		parent::_initialize();

	}

	public function index(){
		layout(false);
		// $msg = mysql_get_server_info();
		\Think\Log::write('当前进行的期数内容');
		$periods = M('period')->select();
		$hasPerioding = false;
		foreach($periods as $period) {
			$status = $period['status'];
			// 如果有进行中
			if($status == C('p_status_go') || $status == C('p_status_ready')) {
				$hasPerioding = true;
				$this->assign('period', $period);		
				break;
			}
		}
		$this->assign('hasPerioding', $hasPerioding);
		$this->display();
	}

	public function welcome(){
		$newOrderTotal = M('order')->where(array('state' => 0))->count();
		$this->assign('newOrderTotal', $newOrderTotal);
		$this->display();
	}

	public function upload(){
		
		$upload = new \Org\Util\UploadFile();// 实例化上传类
		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg','doc','docx','txt','xls','zip','rar');// 设置附件上传类型
		$upload->savePath =  './Public/Uploads/';// 设置附件上传目录
        //生成缩略图后是否删除原图 
		if(!$upload->upload()) {// 上传错误提示错误信息
		    $error = $upload->getErrorMsg();
		    $this->ajaxReturn(0,$error ,0);
		}else{// 上传成功
		    $loadinfo = $upload->getUploadFileInfo();
		    $file_m =  M("files",'crm_cs_','db_config');
		    $res = $file_m->add(upload_data($loadinfo));
		    $this->ajaxReturn(0,$res,1);
		}
	}

	public function _no_check_user(){
		
	}

/*	public function upload(){

		import('ORG.Net.UploadFile');
		$upload = new UploadFile();// 实例化上传类
		$upload->maxSize  = 100000000 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->savePath =  './Public/Uploads/';// 设置附件上传目录

		$upload->thumb = true; //设置需要生成缩略图的文件后缀 
		$upload->thumbPrefix = 'm_';  //生产2张缩略图 
		//设置缩略图最大宽度 
		$upload->thumbMaxWidth = '80'; 
		//设置缩略图最大高度 
		$upload->thumbMaxHeight = '80'; 
		//$upload->thumbRemoveOrigin = true;             //生成缩略图后是否删除原图 
		if(!$upload->upload()) {// 上传错误提示错误信息
		    $error = $upload->getErrorMsg();
		    //echo "上传错误";
		    $this->ajaxReturn(0,$error ,0);
		}else{// 上传成功
		    //$this->success('上传成功！');
		    $lodaInfo = $upload->getUploadFileInfo();
		    \Think\Log::write("loadInfo=>" . $lodaInfo[0]['savename'] ."count=>" . count($lodaInfo));
		    $data['data'] = C('upload_url').'m_'.$lodaInfo[0]['savename'];
		    $data['info'] = $lodaInfo[0]['savename'];
		    $data['status'] = 1;
		    $this->ajaxReturn($data);
		    
		}
	}
*/
}