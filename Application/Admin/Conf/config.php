<?php
return array(
	//'配置项'=>'配置值'
	'LAYOUT_ON'             => true, // 是否启用布局
	'super_user'            => 'admin',
	'super_password' 		=> '123456',
    'super_openid'          => 'oW_r_tjYBp8-_JISbS5HUt73xqro',
    //'super_openid'          => 'oW_r_tv_QGCvyMmrmW55gUpQ2vPc',
    //'LAYOUT_NAME'           =>  'amdin_layout.html', // 当前布局名称 默认为layout

    //'SHOW_PAGE_TRACE'       =>  true, //注意，Trance开启后，日志会被关闭
   
    'TMPL_ACTION_ERROR'		=> '/dispatch_jump.tpl',
    'TMPL_ACTION_SUCCESS'	=> '/dispatch_jump.tpl',
    'URL_HTML_SUFFIX'		=> '',
    /* 概要说明 */ 
    // Public/images/logo.png 需要替换 尺寸像素 214*29
    
    /* 按照产品配置 */
    /*
    'pom_admin_title' 	=> '产品订购微信平台后台管理系统',
    'pom_admin_type_name'	=> '订购',
    'pom_admin_product_name' => '产品',
    */
    /* 按照菜品配置 */
    
    'pom_admin_title' 			=> '九亩点菜后台管理系统',
    'pom_admin_type_name'		=> '点菜',
    'pom_admin_product_name' 	=> '菜品',
    
    'collocation_name'	=> 'A',
    
    /* 	指定鸡蛋、土鸡在xls中的顺序 */
    'xls_new_sort'		=> array(
    	'鸡蛋' 	=> 0,
    	'土鸡'	=> 1
    ),

    'except_option_to_del_food' => array(
        '鸡蛋', '土鸡'
    ),
);