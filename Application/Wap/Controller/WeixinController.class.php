<?php
namespace Wap\Controller;
use Think\Controller;
use Org\Util\Wechat;

class WeixinController extends Controller {

	private $weObj;
	
	public function info() {
		echo "info";
	}
	
	public function index() {
		//import("ORG.Wechat");
		//\Think\Log::write("Wap->Wexin->index");
		//require "Wechat.class.php";
		
 		$this->weObj = new Wechat();
 		//$this->weObj->valid(); //明文或兼容模式可以在接口验证通过后注释此句，但加密模式一定不能注释，否则会验证失败
 		//exit();
 		
 		$type = $this->weObj->getRev()->getRevType();
 
		\Think\Log::write("接收到的事件类型 = " . $type, 'INFO');
	
 		switch($type) {
 		
	 		case Wechat::MSGTYPE_TEXT:
	 			$text = trim($this->weObj->getRev()->getRevContent());
	 			\Think\Log::write("原生 " . $text);
	 			$jen_text = json_encode($text);
	 			\Think\Log::write("json_encode后 = " .$jen_text);
	 			$h = 0xd83d; //高位 
	 			$l = 0xdc94; //低位 
	 			$code = ($h - 0xD800) * 0x400 + 0x10000 + $l - 0xDC00; // 转换算法
	 			\Think\Log::write("code => " . $code);
	 			$textHex = "0x" . strtoupper(dechex($code)); 
	 			// 将textHex 的 16进制转成 10进制
	 			\Think\Log::write("U编码 = " . $textHex);
	 			
	 			$tenText = hexdec($textHex);
	 			\Think\Log::write("转成十进制后 = " . $tenText);
	 			
	 			$a = 0x1F494;
	 			\Think\Log::write("16进制直接 " . $a);
	 			
	 			
	 			\Think\Log::write('-----------------------------');
	 			$h = 0xfe0f;
	 			$h = 0x1fc1e*2;
	 			$l = 0x20e3;
	 			$code = ($h - 0xd800)*0x400 + 0x10000 + $l - 0xdc00;
	 			$upcode = "U+" . strtoupper(dechex($code));
	 			\Think\Log::write("up之后 " . $upcode);
	 			
				$openid = $this->weObj->getRevFrom();
				$myopenid = $this->weObj->getRevTo();
				\Think\Log::write('来自：' . $openid . ', 接受：' . $myopenid, 'INFO');
				\Think\Log::write("文字内容 - " . $text . " <<< " . $openid, 'INFO');
				$replyTxt = "";
				
				switch($text) {
					case "aiyou":
						$url = C('site_url') . U('Yzm/Yzm/iyou') . '/openid/'. $openid;
						$replyTxt = '<a color="#7b7b7b" href="' . $url . '"> i有 </a>';
						break;
					case "yousha":
					
						break;
					default:
						$url = C('site_url') . U('Yzm/Yzm/ys').'/openid/' .$openid;
						$replyTxt = '<a color="#7b7b7b" href="' .$url . '"> YS </a>';
						break;
				}
				
				$news_arr = array(
							"0"	=> 	array(
								"Title"			=> "欢迎来到B2C2C！",
								"Description"	=> "来吧来吧！",
								"PicUrl"		=> "https://mmbiz.q\Think\Logo.cn/mmbiz/Zn4SaaKh2w580Ot2bQctDyAaDWnArtvECFcqUkVibfWQI4JqibVCiakVdCS8YVtFBm6iaGTeZWxEUjUHtHmKwic0mibw/0?wx_fmt=png",
								"Url"			=> "http://m.1kzq.com"
							)
						);
				
				//$this->weObj->text("文字内容 - " . $replyTxt . " <<< " . $openid)->reply();
				//$this->weObj->news($news_arr)->reply();
	 			break;
	 		case Wechat::MSGTYPE_EVENT:
	 			$revEventArr = $this->weObj->getRevEvent();
	 			
	 			switch ($revEventArr['event']) {
		 			case "subscribe":
		 				$openid = $this->weObj->getRevFrom();
		 				$userInfo = $this->weObj->getUserInfo($openid);
		 				
		 				$wxuser = M('wxuser')->where(array('openid' => $userInfo['openid']))->find();
		 				if($wxuser) {
		 					// 如果已存在用户
			 				$data = array('status' => 1);
			 				M('wxuser')->where(array('openid' => $userInfo['openid']))->save($data);
		 				} else {
			 				$newUser = array(
								'openid'			=> $userInfo['openid'],
								'nickname'			=> $userInfo['nickname'],
								'headimgurl'		=> $userInfo['headimgurl'],
								'sex'				=> $userInfo['sex'],
								'province'			=> $userInfo['province'],
								'city'				=> $userInfo['city'],
								'subscribe_time'	=> time(),
								'status'			=> 1,	// 订阅状态
								'discount_money'	=> 0,
							);
							$result = M('wxuser')->add($newUser);
							\Think\Log::write("插入9亩 wxuser用户表");
						}
		 				//$arr = S('access_token_'.C('appid'));
		 				//\Think\Log::write('arr = ' . dump($arr, false), 'INFO');

		 				//$this->subAddUser($userInfo);
						//$this->weObj->news($news_arr)->reply();
						//$this->weObj->text("欢迎关注九亩公众号!")->reply();
		 				break;
		 			case "unsubscribe":
		 				\Think\Log::write("取消关注 ");
		 				$openid = $this->weObj->getRevFrom();
		 				
		 				$data = array('status' => 0);

		 				$res = M('wxuser')->where(array('openid' => $openid))->save($data);
		 				\Think\Log::write("res => " . dump($res, false));
		 				break;
		 			case "TEMPLATESENDJOBFINISH":
		 				$openid = $this->weObj->getRevFrom();
		 				$getData = $this->weObj->getRevData();
		 				$msgid = $getData['MsgID'];
		 				$status = $getData['Status'];
		 				\Think\Log::write("接收finish-》openid =》" . $openid);
		 				\Think\Log::write("finish result =>" . dump($getData, false));
		 				$data = array("pushmsgresult" => $status);
		 				M('wxuser')->where(array('openid' => $openid, 'msgid' => $msgid))->save($data);
		 				break;
		 			default:
		 				break;
	 			}
	 			
	 			break;
	 		default:
	 			$this->weObj->text("help info")->reply();
	 	}
 	}
 	
 	public function getMenu() {
	 	$this->weObj = new Wechat();
	 	$menu = $this->weObj->getMenu();
	 	
	 	var_dump($menu);
 	}
}
?>