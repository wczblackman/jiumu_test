<?php
namespace Wap\Controller;
use Think\Controller;

class UsercenterController extends Controller {
	public $user;
	public function _initialize() {
		\Think\Log::write('9mu -> Wap -> Usercenter -> #_GET[loginUserId] = ' . $_GET['loginUserId']);
		if(!empty($_GET['loginUserId'])) {
			\Think\Log::write('*-----------usercenter 列表进去，有GET-》loginUserId---------------*');
			\Think\Log::write("有GET-》loginUserId");
			\Think\Log::write("GET loginUserId => ".$_GET['loginUserId']);
			\Think\Log::write("请求内容 =>" . C('SERVER_URL').'/user/getByUserId?userId='.$_GET['loginUserId'] );
			$userinfo = httpGet(C('SERVER_URL').'/user/getByUserId?userId='.$_GET['loginUserId']);	// 0:使用非代理
			\Think\Log::write("userinfo => ". dump($userinfo, false));
			//$userinfo = json_decode($userinfo);
			\Think\Log::write("decode userinfo => ". dump($userinfo, false));
			$user = $userinfo->info;	//user->nickname user->userId user->headimgurl
			if ($userinfo->code != 200) {
				header("Content-type: text/html; charset=utf-8");
				echo "获取系统用户信息失败！";
				exit();
			}
			// 9mu用户注册
			$wxuser = M('wxuser')->where(array('openid' => $user->userId))->find();
			if(empty($wxuser)) {
				$newUser = array(
					'openid'			=> $user->userId,
					'nickname'			=> $user->nickname,
					'headimgurl'		=> $user->head,
					'subscribe_time'	=> time(),
					'status'			=> 1,	// 订阅状态
				);
				$result = M('wxuser')->add($newUser);
				\Think\Log::write("插入9亩 wxuser用户表");
			} else {
				\Think\Log::write("已有用户，不用插入wxuser表");
			}
			
			session('openid', $user->userId);
		} else {
			\Think\Log::write('*-----------非 usercenter 列表进去，即无参数GET[loginUserId]---------------*');
			\Think\Log::write("无GET-》loginUserId");
		}
	}
}
?>