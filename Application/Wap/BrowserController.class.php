<?php  
namespace Wap;
use Think\Controller;
/**
* 
*/
class BrowserController extends Controller
{
	function _initialize(){
		//parent::_initialize();
		//
		
		$agent=$_SERVER['HTTP_USER_AGENT'];
		$isWx=strpos($agent, "MicroMessenger");
 		if ($isWx != false) {
 			session('isWx',1);
 		}
		\Think\Log::write('isWx:'.$isWx,false);
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == "com.chint.mychint"){
			session('isApp',1);
		}
		\Think\Log::write('isApp:'.$_SERVER['HTTP_X_REQUESTED_WITH'],false);
	}
}

?>