<?php
namespace Wap\Controller;
use Think\Controller;
use Org\Util\Wechat;
//import("ORG.Wechat");
//import("ORG.ChintOAuth");
/**
 * 用户入口检测试类
 */
class WapController extends Controller{
    public $openid;	
    public $userinfo;
    public $ctoauth;
    
    public function _initialize() {
    	\Think\Log::write("Wap->initialize!");
    	if(!session('openid')) {
    		$selfUrl = getSelfUrl();
        \Think\Log::write('selfUrl => ' . $selfUrl);
        $this->ctoauth = new \Org\Util\Wechat();
        $codeUrl = $this->ctoauth->getOauthRedirect($selfUrl);
        \Think\Log::write('codeUrl => ' . $codeUrl);
        if(empty($_GET['code']) && empty($_GET['state'])) {
         	\Think\Log::write('tiaozhuan codeurl');
          header("Location:$codeUrl");
          exit();
        } 
        \Think\Log::write('there is Code');
        $authResult = $this->ctoauth->getOauthAccessToken($selfUrl);
        \Think\Log::write('authResult => ' . dump($authResult, false));
        $openid = null;
        if($authResult) {
        	// 从ctoauth获取到用户信息
         	$wxuserinfo = $this->ctoauth->getOauthUserinfo($authResult['access_token']);
         	\Think\Log::write('userinfo from ctoauth => '. dump($wxuserinfo, false));
         	if($wxuserinfo) {
         		// 数据库无用户信息，进行信息获取
         		$this->openid = $wxuserinfo['userId'];
         		\Think\Log::write("数据库获取用户之前 this-openid =".$this->openid);
         		$where = array("openid" => $this->openid);
         		$this->userInfo = M("wxuser")->where($where)->find();
         		if($this->userInfo == null) {
         			$newUser = array(
			         'openid'          => $wxuserinfo['userId'],
			         'nickname'      	 => $wxuserinfo['nickname'],
			         'headimgurl'    	 => $wxuserinfo['headimgurl'],
			         'subscribe_time'  => time(),
			         'status'      		 => 1, // 订阅状态
			        );
	      			$result = M('wxuser')->add($newUser);
	      			if($result == true) {
	       				\Think\Log::write("insert into success", 'INFO');
			        } else {
			         	\Think\Log::write("insert into fail", 'INFO');
			        }
         		} 
         		session('openid', $this->openid);
         	} else {
         		$this->error("ChintOAuth认证出错！");
         		exit();
         	}
        } else {
        	header("Content-type: text/html; charset=utf-8"); 
          echo "用户信息获取失败！";
          exit();
        }
      }
      $this->openid = session('openid');
      $where = array('openid' => $this->openid);
      $this->userinfo = M("wxuser")->where($where)->find();
      $this->assign("openid", $this->openid);
      \Think\Log::write('userinfo => ' . dump($this->userinfo, false));
    }
    
    
    public function initialize_old(){
      //session(null);
      //$this->redirect(GROUP_NAME.'/Auth/test');
      /*
      if(session('openid')){
		$user_model = M("wxuser");
          $where = array('openid'=>session('openid'));
          $this->userinfo = $user_model->where($where)->find();
          $this->openid = session('openid');
          \Think\Log::write('userinfo => ' . dump($this->userinfo, false));
          
          if(!$this->userinfo){
              $this->error("用户出错，请关闭窗口后，重新链接");
          }
      } else {
      */
      \Think\Log::write("user:".session('user'));
      \Think\Log::write("userId:".$_GET['userId'],"info");
      \Think\Log::write("openid:".session('openid'),"info");
      \Think\Log::write("session:".session_save_path(),"info");
      $openid = $_GET['userId'];
      if(!session('openid')) {
        \Think\Log::write("session-openid is null");
        $this->openid = $openid;
        if($openid == null){
          //header("Content-type: text/html; charset=utf-8"); 
          //echo "请登陆！";
          $this->error("用户数据错误，请重新登陆！");
          exit();
        }

        $user_model = M("wxuser");
        $where = array('openid' => $this->openid);
        $this->userinfo = $user_model->where($where)->find();
        \Think\Log::write('userinfo => ' . dump($this->userinfo, false));
        session('openid', $this->openid);
        if(!$this->userinfo){
          $msg = $this->httpGet("http://testmboa.chint.com:8080/usercenter/user/getByUserId?userId=".$openid);
          if($msg->code != 200){
            $this->error("错误用户Openid，请重新登陆！");
            exit();
          }
          $user = $msg->info;

          $newUser = array(
             'openid'            => $user->userId,
             'nickname'          => $user->nickname,
             'headimgurl'        => $user->headimgurl,
             'subscribe_time'    => time(),
             'status'            => 1,   // 订阅状态
          );
          \Think\Log::write('newUser => ' . dump($newUser, false));

          $result = M('wxuser')->add($newUser);

          session('openid', $this->openid);
        
          if($result == true) {
            \Think\Log::write("成功插入", 'INFO');
          } else {
            \Think\Log::write("失败插入", 'INFO');
          }
          $user_model = M("wxuser");
          $where = array('openid' => $this->openid);
          $this->userinfo = $user_model->where($where)->find();
          \Think\Log::write('userinfo => ' . dump($this->userinfo, false));
          if(!$this->userinfo){
              $this->error("用户出错，请关闭窗口后，重新链接");
              exit();
          }
        }
        $this->openid = session("openid");
        $this->assign("userinfo", $this->userinfo);
        $this->assign("openid", $this->openid);
      } else {
        $this->openid = session("openid");
        $this->assign("openid", $this->openid);
        \Think\Log::write('header:'.$_SERVER['HTTP_COOKIE'],'info');
      }
    }

    private function httpGet($url){
        //初始化
        $ch = curl_init();
        //设置选项，包括URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

            /* 新增代理部分 */
          if(C('CHINT_AGENT')) {
            $proxy = C('AGENT_URL');
            $proxyport = C('AGENT_PORT');
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
            curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
          }
        //执行并获取HTML文档内容
        $output = curl_exec($ch);
        //释放curl句柄
        curl_close($ch);
        \Think\Log::write('output:'.$output,'INFO');

        $responseBlock = explode("\r\n\r\n",$output);
        $response_body = $responseBlock[count($responseBlock)-1];

        \Think\Log::write('body:'.$response_body,'INFO');

        $msg = json_decode($response_body);
        return $msg;
    }
}