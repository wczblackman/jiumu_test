<?php
// po => product order module
return array(
	//'配置项'=>'配置值'
	/*
	 * 从日常情况看，产品 菜品 商品各有不同的措辞和发货时间要求
	 * 产品称为发货 菜品称为配送
	 * 产品有要求发货时间 菜品有要求配送时间
	 */
	'kconoff' => 0, 	// 是否开启库存
	
	/* 产品配置 */
	'pom_type' => '1',	// 0:产品 1:菜品， 用于区别下单和我的订单页面 显示 什么 时间类型：送达时间 下单时间
	/*
	'pom_type_name' => '产品', // 菜品 产品
	'pom_order_page_title' => '产品订购',		// 订购页的标题
	'pom_first_tab_title' => '产品目录', 	// 第一个tab标题
	'pom_unit' => '件',	// 单位
	'pom_order_done_message' => '将马上为您发货！',
	'pom_delivery_name' => '发货',
	'pom_receive_name' => '收货',	// 确认订单页面
	*/
	/* 菜品配置 */
	
	'pom_type_name' => '菜品', // 菜品 产品
	'pom_order_page_title' => '菜品订购',		// 订购页的标题
	'pom_first_tab_title'  => '菜品目录', 	// 第一个tab标题
	'pom_unit' => '份',	// 单位
	'pom_order_done_message' => '请静候佳肴！',
	'pom_delivery_name' => '配送',
	'pom_receive_name' => '配送',
	
	// info.html 展示图标
	'ICON_INFO'		=> 'icon-info',	// 订单完成
	'ICON_SUCCESS'	=> 'icon-success',
	'ICON_WAITING'	=> 'icon-waiting',
);