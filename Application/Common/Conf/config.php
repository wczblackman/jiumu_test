<?php
return array(
	//'配置项'=>'配置值'
    //微信公众号信息设置
    'token'=>'9munet69a48b295a9e6173', 
    /*
    'appid'=>'wx69a48b295a9e6173', 
    'appsecret'=>'1c98510fd7d5f6d1b6a326a4b195674a', 
    */
    'site_url' => 'http://'.$_SERVER['HTTP_HOST'],
    'appid'=>'wx1687e146c486dde3',
    'appsecret'=>'15f515f87182ff8a7ccf896a39c3ec87',
    'encodingaeskey'=>'AqZWUddgyUWij5iqle53EFTiiMaOBguF7TIZhs31Vlq',
    
    'DB_TYPE'               =>  'mysql',     // 数据库类型
    'DB_HOST'               =>  '127.0.0.1', // 服务器地址
    'DB_NAME'               =>  '9mu',          // 数据库名
    'DB_USER'               =>  'root',      // 用户名
    'DB_PWD'                =>  'Chint!9Mu?',          // 密码
    'DB_PORT'               =>  '6603',        // 端口
    // 'DB_PWD'                =>  '123456',          // 密码
    // 'DB_PORT'               =>  '3306',        // 端口
    'DB_PREFIX'             =>  'tp_',    // 数据库表前缀

    'SESSION_PREFIX'        =>  '9mu_', // session 前缀
    'COOKIE_PREFIX'			=>	'9mu_',
    //邮箱配置（固定）
    'mail_smtp' =>  TRUE,
    'smtp_auth'   =>  TRUE,
    'smtp_secure'  =>  'tls',//tls or ssl                                  
    'mail_charset' =>  'utf-8',
    'smtp_ishtml'  =>  TRUE,

    // 二级目录名，即功能名
    'MODULE_NAME'=> '9mu',
    // 订单状态
    'UNSEND'	=> 1,	// 未收货
    'SENDED'	=> 2,	// 已发货
    'RECEIVED'	=> 3,	// 已收货
    
    'ORDER_MAX' => 6,  // 允许点购最大数量

    'SERVER_URL' => 'http://localhost:9090/cwpu',
    //'SERVER_URL' => 'http://10.1.110.77:8080/cwpu',
    
    // 用于判断项目是否接入公共跳转类，否则进行自己的微信网页授权获取用户信息
    // 一般是用于WapController对于用户信息的获取
    'JOIN_COMMON_OAUTH' 	=> 0,
    'JOIN_COMMON_OAUTH_URL' => 'http://'.$_SERVER['HTTP_HOST'].'/OAuthCenter/index.php?url=',
    
    // 是否代理
    'CHINT_AGENT'   => 0,
    'AGENT_URL'     => 'devtmg.chint.com',
    'AGENT_PORT'    => '80',
    
    // 期数状态
    'p_status_unopen' 	=> 0,
    'p_status_ready'	=> 1,
    'p_status_go'		=> 2,
    'p_status_close'	=> 3,
    
    'collocation_names'	=> array(
    	'A1', 'A2', 'A3', 'A4', 'B1', 'B2','B3','B4','C1','C2','C3','C4',
    ),
    /*
    'cbp_names_arr'	=> array(
	    '3'	=> 'A',
	    '4'	=> 'B',
	    '5'	=> 'C',
    ),
    
    'collocation_A' => array(
    	'6' => 'A1',
    	'7'	=> 'A2',
    	'8'	=> 'A3',
    ),
    */
    
    // 查询权限
    'query_order_priv_all'	=> 'QueryOrderAll',		// 查询所有订单
    'query_order_priv_self'	=> 'QueryOrderSelf',	// 查询自己订单
    
    'PrilgCode_OK'					=> 0,
    'PrilgCode_ERR_NOT_UR_ORDER'	=> 1,
    'PrilgCode_ERR_NULL_ORDERID'	=> 2,
    
    // 配送人员只能修改这两个字段
    'deliver_update_columns'	=> 'state,delivermark',
    
    // 是否展示系统消息
    'showSysMsg'	=> 0,
    'sysMsgTitle'	=> '您好，由于国庆节假期，原定于10月3日停送一次，10月7日周一的配送改为10月8日（周二）配送。请知悉，如果不需要配送两份蔬菜，请记得告知或者点击取消订单，提前预祝国庆节快乐！',	// 额外加到推送模板消息中的title
);
