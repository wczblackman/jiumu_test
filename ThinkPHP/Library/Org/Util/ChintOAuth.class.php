<?php
namespace Org\Util;
class ChintOAuth
{

	const OAUTH_PREFIX = 'http://10.1.110.77:8080/cwpu';
	//const OAUTH_PREFIX = 'http://testmboa.chint.com:8080/cwpu';
	const OAUTH_AUTHORIZE_URL = '/authorize?';
	const OAUTH_TOKEN_URL = '/accessToken';
	const OAUTH_USERINFO_URL = '/authUserInfo?';
	private $token;
	private $client_id;
	private $client_secret;
	private $access_token;
	private $user_token;
	private $postxml;
	public $errCode = 40001;
	public $errMsg = "no access";

	public function __construct($options) {
		$this->client_id = isset($options['clientid'])?$options['clientid']:C('clientid');
		$this->client_secret = isset($options['clientsecret'])?$options['clientsecret']:C('clientsecret');
		//Log::write("C[clientid] >".C('clientid'));
		//Log::write("appid->".C("appid"));
	}
	
	/**
	 * oauth 授权跳转接口
	 * @param string $callback 回调URI
	 * @return string
	 */
	public function getOauthRedirect($callback) {
		//Log::write('getOauthRedirect => ' . $this->client_id);
		return self::OAUTH_PREFIX.self::OAUTH_AUTHORIZE_URL.'client_id='.$this->client_id.'&redirect_uri='.urlencode($callback).'&response_type=code';
	}
	
	/**
	 * 通过code获取Access Token
	 * @return array {access_token,expires_in,refresh_token,openid,scope}
	 */
	public function getOauthAccessToken($callback){
		//Log::write('通过code 获取 access_token: GET[CODE] ='.$_GET['code']);
		$code = isset($_GET['code'])?$_GET['code']:'';
		$redirect_uri = isset($_GET['redirect_uri'])?$_GET['redirect']:'';
		if (!$code) return false;
		$redirect_uri = urlencode($callback);
		$paramArr = array("client_id"=>$this->client_id,
								"client_secret"=>$this->client_secret,
								"code"=>$code,
								"grant_type"=>"authorization_code",
								"redirect_uri"=>urlencode($callback),
		);
		
		
		$aPOST = array();
		foreach($paramArr as $key=>$val){
			$aPOST[] = $key."=".urlencode($val);
		}
		$strPOST =  join("&", $aPOST);
		
		\Think\Log::write("CHINTOAUTH -> 获取accesstoken strPOST=>".self::OAUTH_PREFIX.self::OAUTH_TOKEN_URL.'?'.$strPOST);
		\Think\Log::write("paramArr =>".dump($paramArr, false));
		
		$result = $this->http_post(self::OAUTH_PREFIX.self::OAUTH_TOKEN_URL.'?'.$strPOST, null);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				//Log::write('errMsg => ' . $this->errMsg);
				return false;
			}
			$this->user_token = $json['access_token'];
			return $json;
		}
		return false;
	}
	
	/**
	 * 获取授权后的用户资料
	 * @param string $access_token
	 * @param string $openid
	 * @return array {openid,nickname,sex,province,city,country,headimgurl,privilege,[unionid]}
	 * 注意：unionid字段 只有在用户将公众号绑定到微信开放平台账号后，才会出现。建议调用前用isset()检测一下
	 */
	public function getOauthUserinfo($access_token){
		$result = $this->http_get(self::OAUTH_PREFIX.self::OAUTH_USERINFO_URL.'access_token='.$access_token);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
	}
	
	/**
	 * GET 请求
	 * @param string $url
	 */
	private function http_get($url){
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
		}
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );

		/* 新增代理部分 */
		/*
		$proxy = C('AGENT_URL');
		$proxyport = "80";
		curl_setopt($oCurl, CURLOPT_PROXY, $proxy);
		curl_setopt($oCurl, CURLOPT_PROXYPORT, $proxyport);
		*/
		
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}

	/**
	 * POST 请求
	 * @param string $url
	 * @param array $param
	 * @param boolean $post_file 是否文件上传
	 * @return string content
	 */
	private function http_post($url,$param,$post_file=false){
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
		}
		if (is_string($param) || $post_file) {
			$strPOST = $param;
		} else {
			$aPOST = array();
			foreach($param as $key=>$val){
				$aPOST[] = $key."=".urlencode($val);
			}
			$strPOST =  join("&", $aPOST);
		}
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($oCurl, CURLOPT_POST,true);
		curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);

		/* 新增代理部分 */
		/*
		$proxy = C('AGENT_URL');
		$proxyport = "80";
		curl_setopt($oCurl, CURLOPT_PROXY, $proxy);
		curl_setopt($oCurl, CURLOPT_PROXYPORT, $proxyport);
		*/
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}
}