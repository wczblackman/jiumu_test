#!/usr/bin/python
# -*- coding: UTF-8 -*-

import random
from crontab import CronTab

f = open("crontab_log_buy", "a")

my_user_cron = CronTab(user=True)

# 构造多少
buy_count = random.randint(5, 10) 

for i in range(buy_count):
    # 执行自动发送消息
    jst_fmin = random.randint(10,59)
    jst_fhour = random.randint(9,23)
    fday = 23
    fmonth = 11

    job_buygorecord = my_user_cron.new(command='curl http://m.1kzq.com/?/mobile/test/index/c_ip/1/ > /dev/null 2>&1');

    job_buygorecord_time = str(jst_fmin) + ' ' + str(jst_fhour) + ' ' + str(fday) + ' ' + str(fmonth) + ' *'
    job_buygorecord.set_comment("执行自动购买 - ")

    f.write("\n购买时间：" + job_buygorecord_time)

    # 统一将三个任务进行enable
    job_buygorecord.setall(job_buygorecord_time)
    job_buygorecord.enable()

    my_user_cron.write()



